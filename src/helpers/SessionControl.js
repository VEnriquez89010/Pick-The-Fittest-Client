import ls from 'local-storage'
export  const getToken = () => {
    return {  
        headers: {
            'Content-Type': 'application/json',
            'jwt-key': ls.get('token')
        }
    };
};



