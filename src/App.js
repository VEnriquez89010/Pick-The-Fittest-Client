import React, { Component } from 'react';
import './App.scss';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import NavigationBar from './components/NavigationBar';
import Sidebar from './components/Sidebar';
import Home from './content/Home';
import Profile from './content/Profile';
import Game from './content/Game';
import Results from './content/Results';
import HowToPlay from './content/HowToPlay';
import Rules from './content/Rules';
import Signup from './content/Signup';
import Login from './content/Login';
import ForgotPass from './content/ForgotPass';
import NewPass from './content/NewPass';
import Logout from './content/Logout';
import Redirect from './content/Redirect';
import Leaderboard from './content/Leaderboard';
import Settings from './content/Settings';
import PrivacyPolicy from './content/PrivacyPolicy';
import TermsAndConditions from './content/TermsAndConditions';
import { ToastContainer } from 'react-toastify';
import CacheBuster from './CacheBuster';

/* Admin */ 

import NavigationBarAdmin from './components-admin/NavigationBar';
import SidebarAdmin from './components-admin/Sidebar';
import HomeAdmin from './content-admin/Home';
import SignupAdmin from './content-admin/Signup';
import LoginAdmin from './content-admin/Login';
import ForgotPassAdmin from './content-admin/ForgotPass';
import NewPassAdmin from './content-admin/NewPass';
import Athletes from './content-admin/Athletes';
import AddAthlete from './content-admin/AddAthlete';
import Games from './content-admin/Games';
import AddGame from './content-admin/AddGame';
import AddResults from './content-admin/AddResults';
import LeaderboardAdmin from './content-admin/Leaderboard';
import LeaderboardList from './content-admin/LeaderboardList';
import Members from './content-admin/Members';
import SettingsAdmin from './content-admin/Settings';
import ls from 'local-storage'

class App extends Component {
  render() {
    return (
      <>
          <ToastContainer 
            position="top-right"
            autoClose={3000}
            newestOnTop={true}
            closeOnClick
          >
          </ToastContainer>
          {!(ls.get('isAdmin'))
          ?
          /* USER */
          <Content>
            <Router>
              {/* <Sidebar></Sidebar> */}
              <Sidebar></Sidebar>
              <NavigationBar></NavigationBar>
              <CacheBuster>
                {({ loading, isLatestVersion, refreshCacheAndReload }) => {
                  if (loading) return null;
                  if (!loading && !isLatestVersion) {
                    refreshCacheAndReload();
                  }
                  
                  return null;
                }}
              </CacheBuster>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/profile" component={Profile} />
                <Route path="/game" component={Game} />
                <Route path="/results" component={Results} />
                <Route path="/how-to-play" component={HowToPlay} />
                <Route path="/rules" component={Rules} />
                <Route path="/signup" component={Signup} />
                <Route path="/login" component={Login} />
                <Route path="/forgot-pass" component={ForgotPass} />
                <Route path="/new-pass" component={NewPass} />
                <Route path="/logout" component={Logout} />
                <Route path="/redirect" component={Redirect} />
                <Route path="/leaderboard" component={Leaderboard} />
                <Route path="/privacy-policy" component={PrivacyPolicy} />
                <Route path="/terms-and-conditions" component={TermsAndConditions} />
                <Route path="/settings" component={Settings} />
                <Route exact path="/admin" component={HomeAdmin} />
                <Route path="/admin/login" component={LoginAdmin} />
                <Route path="/admin/forgot-pass" component={ForgotPassAdmin} />
                <Route path="/admin/new-pass" component={NewPassAdmin} />
              </Switch>
            </Router>
            {/* <Breadcrumbs></Breadcrumbs> */}
          </Content>
          /* USER AUTHENTICATION */
           :  
           /* ADMIN */ 
           <Content>
            <Router>
             {/* <SidebarAdmin></SidebarAdmin> */}
              <SidebarAdmin></SidebarAdmin>
              <NavigationBarAdmin></NavigationBarAdmin>
              <CacheBuster>
                {({ loading, isLatestVersion, refreshCacheAndReload }) => {
                  if (loading) return null;
                  if (!loading && !isLatestVersion) {
                    refreshCacheAndReload();
                  }
                  
                  return null;
                }}
              </CacheBuster>
              <Switch>
                <Route exact path="/admin" component={HomeAdmin} />
                <Route path="/admin/signup" component={SignupAdmin} />
                <Route path="/admin/login" component={LoginAdmin} />
                <Route path="/admin/forgot-pass" component={ForgotPassAdmin} />
                <Route path="/admin/logout" component={Logout} />
                <Route path="/admin/new-pass" component={NewPassAdmin} />
                <Route path="/admin/athletes" component={Athletes} />
                <Route path="/admin/add-athlete" component={AddAthlete} />
                <Route path="/admin/games" component={Games} />
                <Route path="/admin/game-creator" component={AddGame} />
                <Route path="/admin/add-results" component={AddResults} />
                <Route path="/admin/leaderboard-list" component={LeaderboardList} />
                <Route path="/admin/leaderboard" component={LeaderboardAdmin} />
                <Route path="/admin/members" component={Members} />
                <Route path="/admin/settings" component={SettingsAdmin} />
                <Route path="/" component={HomeAdmin} />
              </Switch>
            </Router>
          </Content>
          }
      </>
    );
  }
}

export default App;
