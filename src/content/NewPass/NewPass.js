import React from 'react';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button, FormGroup } from 'carbon-components-react';
import Agreement from '../../components/Agreement';
import appSettings from '../../helpers/AppSettings';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const marginForm = { marginBottom: '0.8rem' };

const NewPasswordProps = {
  classNameName: 'some-className',
  id: 'test3',
  labelText: 'Password',
  invalid: false,
  invalidText: '6 letters + 1 digits.',
  placeholder: '6 characters + 1 digit',
};

const buttonEvents = {
  className: 'buttonAccess'
};

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      id: props.location.pathname.split('/').pop()
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event){
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event){
    event.preventDefault();
    let {id, password} = this.state;
    axios.post(`${appSettings.SERVER_URL}/users/restore-password`, {Id: id, Password: password})
    .then(function (response) {
      if(response.data.message === 'succes'){
        //--- cambio de contraseña exitoso, redirigir a algun lugar, login en este caso
        toast("Password reset successfully.");
        setTimeout(() => {
          window.location.href='/login'
          }, 4000);
      } else {
        // --- fallo el cambio de contraseña, mostrar un toast de error (id invalido)
        toast("Error");
      }
    }).catch();
  }

render() {
    return (
      <>
      <div className="authentication">
        <div className="bx--grid">
        <div className="centerVertically">
        <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
            <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
              <h3>
                  New password
              </h3>
              <Form>
              <FormGroup>
                <TextInput
                  style={marginForm}
                  type="password"
                  name='password'
                  value={this.state.value} 
                  required
                  pattern="(?=.*\d)(?=.*[a-z]).{7,}"

                  {...NewPasswordProps} 
                  onChange={this.handleChange}
                />
  
                <Button type="submit"{...buttonEvents} onClick={this.handleSubmit}>
                  Confirm
                </Button>
              </FormGroup>
              </Form>

              <Agreement></Agreement>

            </div>
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        </div>
        </div>
        </div>
      </div>
      </>
    );
  };
}

export default Login;
