import React from 'react';
import './TermsAndConditions.scss';
import Footer from '../../components/Footer';


const TermsAndConditions = () => {
  return (
    <>
    <div className="studioCopy">
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-10 bx--col-md-8">
            <h1>
              Terms and conditions
            </h1>
            <br></br>
            <br></br>
            <p>
            Los términos y condiciones descritos en el presente serán aplicables para el uso 
            del sitio web www.pickthefittest.com y en particular, para utilizar y recibir los Servicios.
            <br></br>
            <br></br>
            <span>
            LE SOLICITAMOS QUE ANTES DE UTILIZAR NUESTROS SERVICIOS, LEA LOS PRESENTES 
            TÉRMINOS Y CONDICIONES.
            </span>
            <br></br>
            <br></br>
            “Pick the Fittest”, como persona moral debidamente constituida y válidamente existente de conformidad con 
            las leyes de los Estados Unidos Mexicanos, inscrita en el Registro Público de 
            Comercio de Cd. Obregón, Sonora, Notario Público numero 40, con ejercicio en 
            este distrito y residencia en esta ciudad. 
            <br></br>
            <br></br>
            El domicilio de Pick the Fittest para 
            todo lo relativo a este Contrato es el ubicado en Morelos y Sinaloa #355 
            Plaza Punto Norte <strong>Wellness Center</strong> local 5.

            <br></br>
            <br></br>
            <strong>
            <span>SERVICIOS QUE PRESTA PICK THE FITTEST</span>
            </strong>
            <br></br>
            <br></br>

            Pick the Fittest ofrece a sus usuarios y clientes servicios consistentes en ejercicios de 
            alto rendimiento, en específico, ejercicios físicos mediante el uso de bicicletas 
            fijas en las clases, horarios y local de Pick the Fittest y en servicios relacionados a 
            dichas clases, incluyendo de asesoría en materia de acondicionamiento físico 
            (“Servicio”).

            <br></br>
            <br></br>
            </p>
            <br></br>
            <br></br>
            <br></br>
            <p>
                <strong><span>Last Updated:</span> </strong>15 June 2020
            </p>
            <br></br>
            <br></br>
            <br></br>
          </div>      
          <div className="bx--col-lg-1 bx--col-md-8"></div>
        </div>          


      </div>
    </div>

    <Footer />
    </>
  );
};
export default TermsAndConditions;
