import React from 'react';
import Footer from '../../components/Footer';
import ls from 'local-storage'
import { Button , Accordion, AccordionItem } from 'carbon-components-react';
import { Draggable24 } from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SortableItem = SortableElement(({value, i}) => 
<li className="DragDropList">
  {(value.ImagePath) 
  ? <img src={value.ImagePath} class="resize" alt=""/>
  : <img src={'./avatar-default.png'} class="resize" alt=""/>
  }
  <span>{value.Name}</span>
  <span className="athlete--position--badge">{i + 1}</span>
  <Draggable24 className="iconDraggable"/>
</li> 
);

const SortableList = SortableContainer(({items}) => {
  return (
    <ul className="DragDropContainer">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value} i={index}/>
      ))}
    </ul>
  );
});

class Profile extends React.Component {
   
  constructor(props) {
    super(props);

    let token = ls.get('token');
   
    if (!token) {
      window.location.href='/login';
      return;
    }

    this.state = {
      name: ls.get('name'),
      items: [],
      tabSelected: '0',
      finalItems: [],
      hasGame: true,
      buttonDisable: true, 
      msg : '',
      gameId : '',
      gameResult: [],
      latestGame: [],
      userId: ls.get('session')
    };

    this.save = this.save.bind(this);
  }

  componentDidMount() {
    this.getInfo();
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    var backupList =this.state.finalItems;
    var tabSelected = this.state.tabSelected;
    var tempList = this.state.finalItems[tabSelected];
    backupList[tabSelected] = arrayMove(tempList, oldIndex, newIndex);

    this.setState({finalItems: backupList, items: backupList[tabSelected] })
  };

  async getInfo(){
    var men, women, teams, gameId = '';
    var hasGame = true;

    var athletes = await axios.get(`${appSettings.SERVER_URL}/games/current-active`);

    if(!athletes.data){
       //  redirect to latest user Results
       var latestGame = await axios.get(`${appSettings.SERVER_URL}/game-results/latest`);
       var result = await axios.get(`${appSettings.SERVER_URL}/game-results/user/${this.state.userId}`);
       
       if(result.data.length > 0){
        this.setState({gameResult: result.data[0], latestGame: latestGame.data.Game});
       }else{
        this.setState({msg: 'No current active games'});
       }

       console.log(result.data.length)

      return;
    }else{
      if(athletes.data && athletes.data.Status === 'ACTIVE'){
        this.setState({buttonDisable: false});
      }
  
      var userBeats = await axios.get(`${appSettings.SERVER_URL}/beat/${ls.get('session')}`, getToken());
  
        if(userBeats.data){
          gameId = userBeats.data.GameId;
          men = userBeats.data.Game[0];
          women = userBeats.data.Game[1]
          teams = userBeats.data.Game[2];
        }else{
          hasGame = false;
          gameId = athletes.data.Id
          men = athletes.data.Game[0];
          women = athletes.data.Game[1];
          teams = athletes.data.Game[2];
        }
  
      this.setState({gameId: gameId, hasGame: hasGame, items: men, finalItems: [men, women, teams]});
    }
  } 

  changeTab(id){
    var defaulAthleteTypes =  this.state.finalItems[id] 
    this.setState({tabSelected: id, items: defaulAthleteTypes});
  }

  async save(){
    this.setState({buttonDisable: true});
    var data = { Id: ls.get('session'), Game: this.state.finalItems , GameId : this.state.gameId};
    var result = await axios.post(`${appSettings.SERVER_URL}/games/user`, data, getToken());

    if(result.data){
      toast("Saved");
      this.getInfo();
      return
    }
    
    toast("Not saved");
    this.setState({buttonDisable: false});
  }

  render() {
    return (
      <>
              <div className="dashBoard" data-type='profile'>
                <div className="bx--grid">
                  <div>

                  <div>
                      <div className="p--no--margin">
                        {/* 
                          <p>Hello</p>
                          <h3>{this.state.name}</h3>
                        */}
                      </div>
                      <br></br>
                      {(this.state.msg === '')
                      ?
                      <div>
                            {(this.state.gameResult && this.state.gameResult.Men != null)
                            ?
                            <div>
                                <h1>Game Results</h1>
                            <div key='1' className="bx--row">
                              <div className="bx--col-lg-12">
                              <div className="total--points">
                                <h3>Total Points: {this.state.gameResult.TotalPoints}</h3>
                              </div>

                              <div className="bx--row">
                                <div className="bx--col-lg-4">
                                <div className="category--points">
                                    <h3>MEN</h3>
                                    <h6>Total Points: {this.state.gameResult.Men[0].Points}</h6>
                                </div>
                                  <div className="bx--row">
                                    <div className="bx--col-lg-6">
                                    <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                      {/* Results */}
                                      <div className="resultsList"> 
                                          <ul>
                                          {this.state.gameResult.Men[0].values.map( (val) => {
                                                return(
                                                  <li> 
                                                    {val.value}   
                                                    <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                                  </li>
                                              )})}
                                          </ul>
                                      </div>
                                    </div>
                                    <div className="bx--col-lg-6">
                                    <div className="resultHeadline">
                                        <p>
                                          Actual Result
                                        </p>
                                      </div>
                                      <div className="resultsList">
                                        <ul>
                                        { this.state.latestGame[0].map( (val) => {
                                          return(
                                            <li>
                                                {val.Name}
                                            </li>
                                          )})}
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              <div className="bx--col-lg-4">
                              <div className="category--points">
                                <h3>WOMEN</h3>
                                <h6>Total Points: {this.state.gameResult.Women[0].Points}</h6>
                              </div>
                                <div className="bx--row">
                                  <div className="bx--col-lg-6">
                                  <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                      {/* Results */}
                                      <div className="resultsList"> 
                                      <ul>
                                        { this.state.gameResult.Women[0].values.map( (val) => {
                                          return(
                                            <li>
                                              {val.value}   
                                              <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                            </li>
                                        )})}
                                    </ul>
                                    </div>
                                  </div>
                                  <div className="bx--col-lg-6">
                                  <div className="resultHeadline">
                                      <p>Actual Result</p>
                                  </div>
                                  <div className="resultsList">
                                    <ul>
                                      { this.state.latestGame[1].map( (val) => {
                                        return(
                                          <li>
                                            {val.Name}
                                          </li>
                                        )})}
                                        </ul>
                                  </div>
                                  </div>
                              </div>
                              </div>

                              <div className="bx--col-lg-4">
                              <div className="category--points">
                                  <h3>TEAMS</h3>
                                  <h6>Total Points: {this.state.gameResult.Teams[0].Points}</h6>
                              </div>
                                <div className="bx--row">
                                  <div className="bx--col-lg-6">
                                  <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                      {/* Results */}
                                      <div className="resultsList">
                                        <ul>
                                    { this.state.gameResult.Teams[0].values.map( (val) => {
                                      return(
                                        <li>
                                          {val.value}
                                          <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                        </li>
                                    )})}
                                    </ul>
                                    </div>
                                  </div>
                                  <div className="bx--col-lg-6">
                                    <div className="resultHeadline">
                                      <p>Actual Result</p>
                                    </div>
                                    <div className="resultsList">
                                      <ul>
                                      { this.state.latestGame[2].map( (val) => {
                                        return(
                                          <li>
                                            {val.Name}
                                          </li>
                                      )})}
                                      </ul>
                                    </div>
                                  </div>
                                  </div> 
                                </div> 
                              </div>
                              </div>
                            </div>
                          </div>
                          :
                          <div className="bx--row">
                            <div className="bx--col-sm-4 bx--col-md-1 bx--col-lg-2"></div>
                            <div className="bx--col-sm-4 bx--col-md-6 bx--col-lg-8">
                            <h1>Pick 'em</h1>
                            <br></br>
                            <br></br>
                          <Tabs
                          className="tabsPosition"
                          defaultTab={this.state.tabSelected}
                          onChange={(tabId) => { this.changeTab(tabId)}}
                        >
                          <TabList>
                            <div  className="rwt__tab">
                            <Tab tabFor="0">Men</Tab>
                            <Tab tabFor="1">Women</Tab>
                            <Tab tabFor="2">Teams</Tab>
                            </div>
                          </TabList>
                          <TabPanel tabId={this.state.tabSelected}>
                              <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />
                          </TabPanel>
                        </Tabs>

                        <Button type="submit" value="Submit" className="buttonAccess" onClick={this.save} disabled={this.state.buttonDisable}>
                            {(this.state.hasGame) ? 'Update game' : 'Save game'}
                        </Button>
                        </div>
                        <div className="bx--col-sm-4 bx--col-md-1 bx--col-lg-2"></div>
                        </div>
                          }
                     
                      </div>
                      :
                      <div>
                         <h1>{this.state.msg}</h1>
                      </div>
                  }
                  </div>

                  </div>
                </div>
              </div>
              <Footer />
              </>
            );
};
}

export default Profile;
