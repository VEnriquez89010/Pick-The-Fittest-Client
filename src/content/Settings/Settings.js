import React from 'react';
import '../../components/LoginControl/LoginControl.scss';
import { Link } from 'react-router-dom';
import { 
  Breadcrumb, 
  BreadcrumbItem,
  Form, 
  TextInput, 
  Button, 
  Checkbox } from 'carbon-components-react';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ls from 'local-storage'
import { getToken } from '../../helpers/SessionControl';
import { Modal } from 'react-responsive-modal';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Name',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Email',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Current password',
  placeholder: ''
};

const NewPasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'New password',
  placeholder: '6 characters + 1 digit'
};


class Settings extends React.Component {
  constructor(props) {
    super(props);
    var userId = ls.get('session');
    this.state = {
      tabSelected: 0, 
      name: '', 
      email: '', 
      newPassword: '', 
      currentPassword: '', 
      isAdmin: true, 
      showPass: false, 
      buttonDisabled: false, 
      userId: userId, 
      openModal : false,
      modalMessage: '',
      isFacebookSession: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.save = this.save.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }

  componentDidMount() {
    this.getInfo();
  }

  async getInfo(){
    var userInfo = await axios.get(`${appSettings.SERVER_URL}/users/id/${this.state.userId}`, getToken());

    if(userInfo.data){
      if(!userInfo.data.Email){
        this.setState({isFacebookSession: true, buttonDisabled: true});
      }

      this.setState({name: userInfo.data.Name, email: userInfo.data.Email});
    }
   
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  save(){
    let msg = '';
  
    if(this.state.tabSelected === '0'){
      msg = 'Are you sure you want to update your general settings?';
    }

    if(this.state.tabSelected === '1'){
      msg = 'Are you sure you want to update your password?';
    }

    this.setState({ openModal: true, modalMessage: msg});
  }

  onCloseModal(){
    this.setState({openModal: false});
  }

  handleSubmit(event) {
    event.preventDefault();
    let currentState = this;
    currentState.setState({buttonDisabled: true });
    let values =  { Id: this.state.userId, Name: this.state.name, Email: this.state.email, CurrentPassword:  this.state.currentPassword, NewPassword: this.state.newPassword, IsAdmin: this.state.isAdmin};

    console.log(this.state.tabSelected)
    axios.put(`${appSettings.SERVER_URL}/users/${this.state.tabSelected}`, values, getToken())
    .then(function (response) {
      
      if(response.data.error){
        currentState.setState({buttonDisabled: false , openModal: false});
        toast(response.data.message);
        return;
      }

        //--- Toast succes. Usuario Añadido correctamente
        toast(response.data.message);
        ls.set('name', response.data.name);
        currentState.setState({openModal: false});
        setTimeout(() =>  window.location.href = '/settings', 4000);
    }).catch(error => {
      //--- Error.
      toast("An unexpected error has occurred.");
    });
  }

  render() {
    return (
      
      <div className="dashBoard">
      <div>
      <div className="bx--grid">
      <div>
        <Modal 
            open={this.state.openModal} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
        >
          <p className="modal--text">
               <strong>
                  {this.state.modalMessage}
               </strong>
          </p>
          <div>
          <Button 
            type="submit" 
            value="Submit" 
            size='small'
            kind='ghost' 
            onClick={this.handleSubmit}
          >
            Confirm
          </Button>
          <Button 
            type="submit" 
            kind='ghost' 
            size='small'
            onClick={this.onCloseModal} 
          >
            Cancel
          </Button>
          </div>
        </Modal>
      </div>
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
              <h1>Settings</h1>
              <br></br>
              <br></br>
              <Tabs
                          className="tabsPosition"
                          defaultTab={this.state.tabSelected}
                          onChange={(tabId) => this.setState({tabSelected: tabId})}
                        >
                          <TabList>
                            <div  className="rwt__tab">
                            <Tab tabFor="0">General</Tab>
                            <Tab tabFor="1">Security</Tab>
                            </div>
                          </TabList>
                          <TabPanel tabId='0'>
                          <Form className="settings--box">
                            <TextInput
                                name='name'
                                type="text" 
                                value={this.state.name} 
                                onChange={this.handleChange} 
                                {...NameInputProps}  
                                disabled={this.state.isFacebookSession}
                              />


                              <TextInput
                                name='email'
                                type="email" 
                                value={this.state.email} 
                                onChange={this.handleChange} 
                                pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
                                
                                {...EmailInputProps}  
                                disabled={this.state.isFacebookSession}
                              />
                          </Form>
                          </TabPanel>
                          <TabPanel tabId='1'>
                          <Form className="settings--box">
                              <TextInput
                                name='currentPassword'
                                type={ !this.state.showPass ? 'password' : 'text'}
                                value={this.state.currentPassword} 
                                onChange={this.handleChange} 
                                pattern="(?=.*\d)(?=.*[a-z]).{7,}"

                                {...PasswordInputProps}  
                                disabled={this.state.isFacebookSession}
                              />

                              <TextInput
                                name='newPassword'
                                type={ !this.state.showPass ? 'password' : 'text'}
                                value={this.state.newPassword} 
                                onChange={this.handleChange} 
                                pattern="(?=.*\d)(?=.*[a-z]).{7,}"

                                {...NewPasswordInputProps}  
                                disabled={this.state.isFacebookSession}
                              />
                              <Checkbox 
                                labelText="Show password" 
                                id="checked" 
                                onChange={() => this.setState({showPass: !this.state.showPass})} />
                          </Form>
                          </TabPanel>
              </Tabs>

              {(this.state.isFacebookSession)
              ?
                <div className="account--access">
                  <span>
                    Email and password unable to be changed because you signed in using 
                    Facebook or Google.
                  </span>
                </div>
              : null }
              


              <br></br>
              <Button 
                type="submit" 
                value="Submit" 
                className="buttonAccess" 
                disabled={this.state.buttonDisabled}
                onClick={this.save}>Save changes</Button>


        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>

      <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/">Home</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/settings">Settings</BreadcrumbItem>
      </Breadcrumb>

      </div>
      </div>
      </div>
    );
  }
}

export default Settings;