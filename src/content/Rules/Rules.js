import React from 'react';
import './Rules.scss';
import sectionImg0 from './shapes.png';
import Footer from '../../components/Footer';


const Rules = () => {
  return (
    <>
    <div className="studioCopy">
      <div className="bx--grid">
      <div className="bx--row">
      <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-5 bx--col-md-4">
              <img src={sectionImg0} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-5 bx--col-md-4">
            <h1>
              Rules
            </h1>
            <p>
            <span>
            For the second year in a row, <strong>Reebok</strong> is bringing the CrossFit community CrossFit Games Pick’Em. Pick’Em allows users to predict the top 10 men, top 10 women and top five teams at the 2018 Reebok CrossFit Games. The more picks you get correct, the higher your score. There is a <strong>$2 million</strong> prize on the line in the event that an entrant submits a perfect prediction. Share with your friends online using #ReebokPickEm and challenge them to play!
            </span>
            <span>
            Toda la información es almacenada de forma segura mediante nuestro firewall.
            </span>
            
            <br></br>
            <br></br>
            <div className="counter--list">
              <p>Prep ingredients and Sauté if required.</p>
              <p>Add ingredients to inner pot.</p>
              <p>Close the lid. Set release to 0.</p>
            </div>

            <br></br>
            <br></br>
            <strong>
            <span>Prizes</span>
            </strong>
            <br></br>
            <br></br>

            While anyone globally can play Pick’Em, only residents of the US, Canada, UK, Australia and New Zealand are eligible to win the prizes.
            
            <br></br>
            <br></br>
            </p>

            <p>
                <strong><span>Last Updated:</span> </strong>15 June 2020  
            </p>
            <br></br>
            <br></br>
            <br></br>
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
        </div>          




      </div>
    </div>

    <Footer />
    </>
  );
};
export default Rules;
