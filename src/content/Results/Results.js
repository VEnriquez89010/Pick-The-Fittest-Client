import React from 'react';
import './Results.scss';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import appSettings from '../../helpers/AppSettings';
import 'react-toastify/dist/ReactToastify.css';
import { 
  Breadcrumb, 
  BreadcrumbItem 
} from 'carbon-components-react';

class Results extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      game: [],
      items: [],
      gameId: props.location.pathname.split('/').pop(),
      tabSelected: '', 
      totalPoints: '',
      latestGame: []
    };

    this.componentDidMount = this.componentDidMount.bind(this);
  }

  async componentDidMount() {
    var latestGame = await axios.get(`${appSettings.SERVER_URL}/game-results/latest`);
    var result = await axios.get(`${appSettings.SERVER_URL}/game-results/id/${this.state.gameId}`);
    this.setState({ game: result.data, latestGame: latestGame.data.Game });
  }

  render() {
    return (
    <>
    <div className="dashBoard">
    <div className="">
            
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-lg-4">
            <h1>Results</h1>
            <br></br>
            <br></br>
          </div>
          <div className="bx--col-lg-4">
          </div>
          <div className="bx--col-lg-4"></div>
        </div>

          <>
          { this.state.game.map( (element, index) => {
                        return(
                          <div key={index} className="bx--row">
                            <div className="bx--col-lg-12">
                            <div className="results">
                            <div>
                            <div className="total--points">
                              
                              <h3>Total Points: {element.TotalPoints}</h3>
                              
                            </div>
                            <div className="bx--row">
                              <div className="bx--col-lg-4">
                                <div className="category--points">
                                <h3>MEN</h3>
                                <h6>Points: {element.Men[0].Points}</h6>
                                </div>
                                <div className="bx--row">
                                  <div className="bx--col-lg-6">
                                      <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                      {/* Results */}
                                      <div className="resultsList"> 
                                      <ul>
                                      {element.Men[0].values.map( (val) => {
                                          return(
                                           
                                                <li>
                                                  {val.value} 
                                                  <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                                </li>
                                             
                                      )})}
                                      </ul>
                                      </div>
                                  </div>
                                  <div className="bx--col-lg-6">
                                      <div className="resultHeadline">
                                        <p>
                                          Actual Result
                                        </p>
                                      </div>
                                      <div className="resultsList">
                                        <ul>
                                        { this.state.latestGame[0].map( (val) => {
                                        return(
                                              <li>
                                                {val.Name}
                                              </li>
                                          )})}
                                        </ul>
                                        </div>
                                  </div>
                                </div>
                              </div>

                            <div className="bx--col-lg-4">
                            <div className="category--points">
                              <h3>WOMEN</h3>
                              <h6>Points: {element.Women[0].Points}</h6>
                              </div>
                              <div className="bx--row">
                                <div className="bx--col-lg-6">
                                <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                  {/* Results */}
                                  <div className="resultsList">
                                    <ul>
                                  { element.Women[0].values.map( (val) => {
                                    return(
                                        <li>
                                          {val.value}
                                          <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                          {/* Pts: {val.points} */}
                                        </li>
                                  )})}
                                  </ul>
                                  </div>
                                </div>
                                <div className="bx--col-lg-6">
                                <div className="resultHeadline">
                                        <p>
                                          Actual Result
                                        </p>
                                      </div>
                                    <div className="resultsList">
                                    <ul>
                                    { this.state.latestGame[1].map( (val) => {
                                      return(
                                          <li>{val.Name}</li>
                                          )})}
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div className="bx--col-lg-4">
                            <div className="category--points">
                              <h3>TEAMS</h3>
                              <h6>Points: {element.Teams[0].Points}</h6>
                              </div>
                              <div className="bx--row">
                                <div className="bx--col-lg-6">
                                <div className="resultHeadline">
                                        <p>
                                          Theirs
                                        </p>
                                      </div>
                                  {/* Results */}
                                  <div className="resultsList">
                                    <ul>
                                  { element.Teams[0].values.map( (val) => {
                                    return(
                                        <li>{val.value}
                                        <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                        </li>
                                        
                                  )})}
                                  </ul>
                                  </div>
                                </div>
                                <div className="bx--col-lg-6">
                                <div className="resultHeadline">
                                        <p>
                                          Actual Result
                                        </p>
                                      </div>
                                    <div className="resultsList">
                                      <ul>
                                    { this.state.latestGame[2].map( (val) => {
                                      return(
                                          <li>{val.Name}</li>
                                          )})}
                                    </ul>
                                    </div>
                                </div>
                                </div> 
                              </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                          </div>
                          );
                        })
                      }
          </>

        <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem href="/admin/Games">Games</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="#">Game Results</BreadcrumbItem>
        </Breadcrumb>
      </div>
    </div>
    </div>
    </>
    );
  }
};
export default Results;
