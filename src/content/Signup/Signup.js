import React from 'react';
import '../../components/LoginControl/LoginControl.scss';
import { 
  Form, 
  TextInput, 
  Button, 
  Checkbox } from 'carbon-components-react';
import Agreement from '../../components/Agreement';
import ls from 'local-storage'
import { getToken } from '../../helpers/SessionControl';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Name',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Email',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Password',
  placeholder: '6 characters + 1 digit',
  invalid: false,
  invalidText:
    'Your password must be at least 6 characters as well as contain at least one uppercase, one lowercase, and one number.',
};


const buttonEvents = { className: 'buttonAccess' };

class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: '', email: '', password: '', showPass: false, buttonDisabled: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  notify = () => toast("Email already taken. Please try another one.");

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({buttonDisabled: true});
    let values =  { Name: this.state.name, Email: this.state.email, Password: this.state.password }  // { Email: obtenEvent }
    axios.post(`${appSettings.SERVER_URL}/users/validate-mail`, { Email:this.state.email})
    .then( existEmail => {
      if(existEmail.status === 200){
        if(!existEmail.data.Available){
          //--- Toast Failed. ya existe correo electronico
          this.notify()
          this.setState({buttonDisabled: false});
          return;
        }else{
          axios.post(`${appSettings.SERVER_URL}/users/add`, values)
          .then(function (response) {
              
            let userID   = response.data.user.id;
            let userName = response.data.user.Name;
            let token = response.data.token
      
            ls.set('session', userID);
            ls.set('name', userName);
            ls.set('token', token);

            // --- Save temporal game if exists
            var tempGameId = ls.get('temp-game-id');

            if(tempGameId){
                var json = JSON.parse(ls.get('temp-game'));
                var data = { Id: userID, Game: json , GameId : tempGameId};
                axios.post(`${appSettings.SERVER_URL}/games/user`, data, getToken());
                ls.remove('temp-game');
                ls.remove('temp-game-id');
            }
            
            window.location.href='/profile';
          }).catch( () => {
            //--- Toast error, no se creo usuario
            toast("Error. User not created.");
            this.setState({buttonDisabled: false});
          });
        }
      }
    }).catch(error => {
       //--- Toast fail. Email error
       toast("Email error.");
       this.setState({buttonDisabled: false});
    });
  }

  render() {

        /*  Facebook and Google URL redirection */
         function facebookSignup(e) {
          e.preventDefault();
            window.location.href= `${appSettings.SERVER_URL}/fb/loginFB`;
        }
         function googleSignup(e) {
          e.preventDefault();
          window.location.href='/google';
        }
    

    return (
      <>
      <div className="authentication">
      <div className="bx--grid">
      <div className="centerVertically">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Create account
              </h3>

              <TextInput
              name='name'
              type="text" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...NameInputProps}  
              />


              <TextInput
              name='email'
              type="email" 
              required
              value={this.state.value} 
              onChange={this.handleChange}
              pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
              
              {...EmailInputProps}  
              />

              <TextInput
              name='password'
              type={ !this.state.showPass ? 'password' : 'text'}
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              pattern="(?=.*\d)(?=.*[a-z]).{7,}"

              {...PasswordInputProps}  
              />
              <Checkbox 
                labelText="Show password" 
                id="checked" 
                onChange={() => this.setState({showPass: !this.state.showPass})} />

              <Button 
                  type="submit" 
                  value="Submit" 
                  className="buttonAccess" data-type="margin--top"
                  disabled={this.state.buttonDisabled}>
                    Sign up
              </Button>
                {/* 
                  <div className="buttonGoogle">
                  <Button onClick={googleSignup} type="submit" {...buttonEvents}>   </Button>
                  </div>
                */}
                  <div className="buttonFacebook">
                  <Button onClick={facebookSignup} type="submit" {...buttonEvents}> </Button>
                  </div>
          </Form>

          <div className="account--access">
            <span>Have an account?
            {' '}
            <Link element={Link} to="/login">
                Sign in
            </Link>
            </span>
          </div>

        <Agreement></Agreement>
        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
      </div>
      </>
    );
  }
}

export default SignupForm;