import React from 'react';
import Footer from '../../components/Footer';
import ls from 'local-storage'
import { Button , Accordion, AccordionItem } from 'carbon-components-react';
import { Draggable24 } from '@carbon/icons-react';
import avatar from './avatar-default.png';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SortableItem = SortableElement(({value, i}) => 
<li className="DragDropList">
  {(value.ImagePath) 
  ? <img src={value.ImagePath} class="resize" alt=""/>
  : <img src={avatar} class="resize" alt=""/>
  }
  <span>{value.Name}</span>
  <span className="athlete--position--badge">{i + 1}</span>
  <Draggable24 className="iconDraggable"/>
</li> 
);

const SortableList = SortableContainer(({items}) => {
  return (
    <ul className="DragDropContainer">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value} i={index}/>
      ))}
    </ul>
  );
});

class Game extends React.Component {
   
  constructor(props) {
    super(props);


    this.state = {
      name: ls.get('name'),
      items: [],
      tabSelected: '0',
      finalItems: [],
      hasGame: true,
      buttonDisable: false, 
      msg : '',
      gameId : '',
      gameResults: [],
      latestGame: []
    };

    this.save = this.save.bind(this);
  }

  componentDidMount() {
    this.getInfo();
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    var backupList =this.state.finalItems;
    var tabSelected = this.state.tabSelected;
    var tempList = this.state.finalItems[tabSelected];
    backupList[tabSelected] = arrayMove(tempList, oldIndex, newIndex);

    this.setState({finalItems: backupList, items: backupList[tabSelected] })
  };

  async getInfo(){
    var gameActive = await axios.get(`${appSettings.SERVER_URL}/games/current-active`);

    if(!gameActive.data){
       //  redirect to latest Results
       var latestGame = await axios.get(`${appSettings.SERVER_URL}/game-results/latest`);
       var result = await axios.get(`${appSettings.SERVER_URL}/game-results/id/${latestGame.data.GameId}`);
       this.setState({msg: 'No Compoetitions', gameResults: result.data, latestGame: latestGame.data.Game });
      return;
    }else{
      if(gameActive.data.Status === 'PAUSE'){
        this.setState({buttonDisable: true});
      }
  
      this.setState({ gameId: gameActive.data.Id ,items: gameActive.data.Game[0], finalItems: [gameActive.data.Game[0], gameActive.data.Game[1], gameActive.data.Game[2]]});
    }
  } 

  changeTab(id){
    var defaulAthleteTypes =  this.state.finalItems[id] 
    this.setState({tabSelected: id, items: defaulAthleteTypes});
  }

  async save(){

    ls.set('temp-game', JSON.stringify(this.state.finalItems));
    ls.set('temp-game-id', this.state.gameId);

    let session = ls.get('token');
   
    if (!session) {
      window.location.href='/login';
      return;
    }

    this.setState({buttonDisable: true});
    var data = { Id: ls.get('session'), Game: this.state.finalItems , GameId : this.state.gameId};
    var result = await axios.post(`${appSettings.SERVER_URL}/games/user`, data, getToken());

    if(result.data){
      toast("Saved");
      this.getInfo();
      return
    }
    
    toast("Not saved");
    this.setState({buttonDisable: false});
  }

  render() {
    return (
      <>
              <div className="dashBoard" data-type='profile'>
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-lg-12">
                      <div>
                        {/* 
                          <p>Pick 'em</p>
                          <h3>Your Game</h3>
                        */}
                      
                      </div>
                      <br></br>
                      <br></br>
                      {(this.state.msg !== '')
                      ?
                      <div>
                        <h1>Game Results</h1>
                        <br></br>
                      <Accordion>
                      { this.state.gameResults.map( (element, index) => {
                                    return(
                                      <div key={index} className="bx--row">
                                        <div className="bx--col-lg-12">
                                        <AccordionItem title={`${index + 1} - ${element.UserName}`}>
                                        <div className="total--points">
                                          <h3>Total Points: {element.TotalPoints}</h3>
                                        </div>
                                        <div className="bx--row">
                                          <div className="bx--col-lg-4">
                                          <div className="category--points">
                                                  <h3>MEN</h3>
                                                  <h6>Total Points: {element.Men[0].Points}</h6>
                                          </div>
                                            <div className="bx--row">
                                              <div className="bx--col-lg-6">
                                              <div className="resultHeadline">
                                                <p>
                                                  Theirs
                                                </p>
                                              </div>
                                              <div className="resultsList"> 
                                                <ul>
                                                    {element.Men[0].values.map( (val) => {
                                                          return(
                                                              <li>
                                                                {val.value} 
                                                                <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                                              </li>
                                                        )})}
                                                </ul>
                                              </div>
                                              </div>
                                              <div className="bx--col-lg-6">
                                              <div className="resultHeadline">
                                                <p>Actual Result</p>
                                              </div>
                                              <div className="resultsList">
                                                <ul>
                                                  { this.state.latestGame[0].map( (val) => {
                                                    return(
                                                        <li>{val.Name}</li>
                                                    )})}
                                                </ul>
                                              </div>
                                              </div>
                                            </div>
                                          </div>
            
                                        <div className="bx--col-lg-4">
                                        <div className="category--points">
                                            <h3>WOMEN</h3>
                                            <h6>Total Points: {element.Women[0].Points}</h6>
                                          </div>
                                          <div className="bx--row">
                                            <div className="bx--col-lg-6">
                                            <div className="resultHeadline">
                                                <p>
                                                  Theirs
                                                </p>
                                              </div>
                                              <div className="resultsList"> 
                                              <ul>
                                              { element.Women[0].values.map( (val) => {
                                                return(
                                                    <li>
                                                      {val.value}
                                                      <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                                      </li>
                                              )})}
                                              </ul>
                                              </div>
                                            </div>
                                            <div className="bx--col-lg-6">
                                            <div className="resultHeadline">
                                              <p>Actual Result</p>
                                            </div>
                                            <div className="resultsList">
                                              <ul>
                                                { this.state.latestGame[1].map( (val) => {
                                                  return(
                                                      <li>{val.Name}</li>
                                                  )})}
                                              </ul>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
            
                                        <div className="bx--col-lg-4">
                                        <div className="category--points">
                                          <h3>TEAMS</h3>
                                          <h6>Total Points: {element.Teams[0].Points}</h6>
                                          </div>
                                          <div className="bx--row">
                                            <div className="bx--col-lg-6">
                                            <div className="resultHeadline">
                                                <p>
                                                  Theirs
                                                </p>
                                              </div>
                                              <div className="resultsList">
                                                <ul>
                                              { element.Teams[0].values.map( (val) => {
                                                return(
                                                    <li>
                                                        {val.value}   
                                                        <span className="scorePoint" data-type={val.points !== 0 ? 'green': 'gray'}></span>
                                                    </li>
                                              )})}
                                              </ul>
                                              </div>
                                            </div>
                                            <div className="bx--col-lg-6">
                                            <div className="resultHeadline">
                                              <p>Actual Result</p>
                                            </div>
                                            <div className="resultsList">
                                              <ul>
                                                { this.state.latestGame[2].map( (val) => {
                                                  return(
                                                      <li>{val.Name}</li>
                                                )})}
                                              </ul>
                                              </div>
                                            </div>
                                            </div> 
                                          </div> 
                                        </div>
                                        </AccordionItem>
                                        </div>
                                      </div>
                                      );
                                    })
                                  }
                      </Accordion>
                      </div>
                      :

                      <div className="bx--row">
                        <div className="bx--col-sm-4 bx--col-md-1 bx--col-lg-2"></div>
                        <div className="bx--col-sm-4 bx--col-md-6 bx--col-lg-8">
                          <h1>Pick 'em</h1>
                          <p>Drag and drop to position the Athletes</p>
                          <br></br>
                          <br></br>
                    <Tabs
                          className="tabsPosition"
                          defaultTab={this.state.tabSelected}
                          onChange={(tabId) => { this.changeTab(tabId)}}
                        >
                          <TabList>
                            <div  className="rwt__tab">
                            <Tab tabFor="0">Men</Tab>
                            <Tab tabFor="1">Women</Tab>
                            <Tab tabFor="2">Teams</Tab>
                            </div>
                          </TabList>
                          <TabPanel tabId={this.state.tabSelected}>
                              <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />
                          </TabPanel>
                        </Tabs>

                        <Button type="submit" value="Submit" className="buttonAccess" onClick={this.save} disabled={this.state.buttonDisable}>
                             Save
                        </Button>
                        </div>
                        <div className="bx--col-sm-4 bx--col-md-1 bx--col-lg-2"></div>
                        </div>
                        }
                  </div>
                  </div>
                </div>
              </div>
              <Footer />
              </>
            );
};
}

export default Game;
