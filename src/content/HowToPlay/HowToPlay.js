import React from 'react';
import './HowToPlay.scss';
import sectionImg0 from './shapes.png';
import Footer from '../../components/Footer';


const HowToPlay = () => {
  return (
    <>
    <div className="studioCopy">
      <div className="bx--grid">
      <div className="bx--row">

          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-5 bx--col-md-4">
              <img src={sectionImg0} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-5 bx--col-md-4">
          <h1>
              How to Play
          </h1>
          <br></br>
          <br></br>
          <p>
            <span>
            For the second year in a row, <strong>Reebok</strong> is bringing the CrossFit community CrossFit Games Pick’Em. Pick’Em allows users to predict the top 10 men, top 10 women and top five teams at the 2018 Reebok CrossFit Games. The more picks you get correct, the higher your score. There is a <strong>$2 million</strong> prize on the line in the event that an entrant submits a perfect prediction. Share with your friends online using #ReebokPickEm and challenge them to play!
            </span>
            <br></br>
            <br></br>
            <strong>
            <span>Make Your Picks</span>
            </strong>
            <br></br>
            <br></br>

            Select the athletes in the order you believe they will finish the 2018 Reebok CrossFit Games at the close of the final event. When you select an athlete, they will automatically be placed in the highest available position on your leaderboard. For example, the first athlete you select will be selected as finishing first. The second athlete will be placed in the second place position, etc. To learn more about an athlete, click on his or her name where you’ll find basic information, including the athlete’s region, affiliate gym and relevant stats. Still having trouble deciding who to pick? This guide may help. You can continue to rearrange your bracket until July 31, 2018, at 11:59 CDT.

            <br></br>
            <br></br>

            <strong>
            <span>Scoring</span>
            </strong>
            <br></br>
            <br></br>

            The closer you are to predicting the final leaderboard, the more points you earn.
            
            <br></br>
            <br></br>
            <ul>
              <li>Correct: 100 points</li>
              <li>One Away: 50 points</li>
              <li>Two Away: 20 points</li>
              <li>Two Away: 20 points</li>
            </ul>

            <br></br>
            <br></br>
            <strong>
            <span>Prizes</span>
            </strong>
            <br></br>
            <br></br>

            While anyone globally can play Pick’Em, only residents of the US, Canada, UK, Australia and New Zealand are eligible to win the prizes.
            
            <br></br>
            <br></br>
            </p>

            <p>
                <strong><span>Last Updated:</span> </strong>15 June 2020  
            </p>
            <br></br>
            <br></br>
            <br></br>

          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
   



      </div>
    </div>

    <Footer />
    </>
  );
};
export default HowToPlay;
