import React from 'react';
import { Link } from 'react-router-dom';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button, Checkbox } from 'carbon-components-react';
import Agreement from '../../components/Agreement';
import ls from 'local-storage'
import axios from 'axios';
import { getToken } from '../../helpers/SessionControl';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Email',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Password',
  placeholder: ''
};

const buttonEvents = { className: 'buttonAccess' };

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: '', showPass: false};
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    let values =  { Email: this.state.email, Password: this.state.password } 
    axios.post(`${appSettings.SERVER_URL}/sessions/login`, values)
    .then(function (response) {
        if(response.status === 200){
          let userID   = response.data.user.id;
          let userName = response.data.user.Name;
          let token = response.data.token

          ls.set('session', userID);
          ls.set('name', userName);
          ls.set('token', token);

          // --- Save temporal game if exists
          var tempGameId = ls.get('temp-game-id');

          if(tempGameId){
              var json = JSON.parse(ls.get('temp-game'));
              var data = { Id: userID, Game: json , GameId : tempGameId};
              axios.post(`${appSettings.SERVER_URL}/games/user`, data, getToken());
              ls.remove('temp-game');
              ls.remove('temp-game-id');
          }
          
          window.location.href='/profile';
        }else{
          //---- Toastr error, not found
          toast("User not found.");
          return;
        }
    })
    .catch(function (error) {
      //---- Toastr error, not found
      toast("Invailid email or password.");
    });

  }


  render() {

        /*  Facebook and Google URL redirection */
         function facebookSignup(e) {
          e.preventDefault();
            window.location.href= `${appSettings.SERVER_URL}/fb/loginFB`;
        }
         function googleSignup(e) {
          e.preventDefault();
          window.location.href=`${appSettings.SERVER_URL}/google`;
        }
    

    return (
      <>
      <div className="authentication">
      <div className="bx--grid">
      <div className="centerVertically">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Account
              </h3>

              <TextInput
              name='email'
              type="email" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...EmailInputProps}  
              />

              <TextInput
              name='password'
              type={ !this.state.showPass ? 'password' : 'text'}
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...PasswordInputProps}  
              />
              <Checkbox 
                labelText="Show password" 
                id="checked" 
                onChange={() => this.setState({showPass: !this.state.showPass})} />

              <Button 
                type="submit" 
                value="Submit" 
                className="buttonAccess" data-type="margin--top">
                  Sign in
              </Button>
                {/* 
                  <div className="buttonGoogle">
                  <Button onClick={googleSignup} type="submit" {...buttonEvents}>   </Button>
                  </div>
                */}

                  <div className="buttonFacebook">
                  <Button onClick={facebookSignup} type="submit" {...buttonEvents}> </Button>
                  </div>

               <Link className="forgotPass" element={Link} to="/forgot-pass">
                Forgot password?
              </Link>
          </Form>

          <div className="account--access">
            <span>New to Pick the fittest?
            {' '}
            <Link element={Link} to="/signup">
                Create account
            </Link>
            </span>
          </div>

          <Agreement></Agreement>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      
      </div>
      </div>
      </>
    );
  }
}

export default LoginForm;