import React from 'react';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button } from 'carbon-components-react';
import Agreement from '../../components/Agreement';
import axios from 'axios';
import ls from 'local-storage'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import appSettings from '../../helpers/AppSettings';

const EmailInputProps = {
  className: 'textInput',
  id: 'forgot--pass',
  labelText: 'Email',
  placeholder: ''
};

const buttonEvents = {
  className: 'buttonAccess'
};

class ForgotPass extends React.Component {

  constructor(props) {
    super(props);
    this.state = {email: '', buttonDisabled: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    if(ls.get('session')){
     window.location.href = '/settings';
    }
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(e) {
    e.preventDefault();
    let currentState = this;
    currentState.setState({buttonDisabled: true});
    let { email } = this.state;
    axios.post(`${appSettings.SERVER_URL}/users/password-recover`,{ Email: email })
    .then(function (response) {
    toast(response.data.message); 
    if(response.data.status === 404){
      currentState.setState({buttonDisabled: false});
      return;
    }
  })
  .catch(function (error) {
    toast("Error.");
    currentState.setState({buttonDisabled: false});
  });
  }

  render() {
    
  
    return (
      <>
      <div className="authentication">
      <div className="bx--grid">
        <div className="centerVertically">
        <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
            <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
              <h3 className="formTitles">
                Password recovery
              </h3>
              <br></br>
              <Form className="some-class">
                <TextInput 
                  name='email'
                  type="email" 
                  required
                  value={this.state.value} 
                  onChange={this.handleChange} 
                  pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"

                  {...EmailInputProps} 
                />
  
                <Button onClick={this.handleSubmit} type="submit"{...buttonEvents} disabled={this.state.buttonDisabled}>
                  Continue
                </Button> 
              </Form>


              <Agreement></Agreement>
            </div>
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        </div>
        </div>
      
      </div>
      </div>
      </>
    );
  };
}


export default ForgotPass;
