import React from 'react';
import './Home.scss';
import logoNoBull from './no-bull-games-logo-2.svg';
import sectionImg0 from './shapes.png';
import sectionImg1 from './ptf-1.png';
import sectionImg2 from './ptf-2.png';
import sectionImg3 from './ptf-3.png';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import Footer from '../../components/Footer';
import ls from 'local-storage'


const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return  <Link element={Link} to="/game">
              <Button className="button--hero">Start now</Button>
            </Link>
  } else {
    return <Link element={Link} to="/game">
              <Button className="button--hero">Start playing</Button>
           </Link>
  }
}

const Home = () => {
  return (
    <>
    {/* comment here */}
      <div className="heroHome">
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-lg-3 bx--col-md-8"></div>
          <div className="bx--col-lg-6 bx--col-md-8">
            <div className="heroHeadline">
              <div className="center">
                <img src={logoNoBull} alt='CrossFit logo' className="current--game--logo" />
                {/* <h1>Pick the Fittest </h1> */}
                <h5 className="hero--space">The place where you can predict the top finishers for different fitness competitions.</h5>

                {renderAuthButton()}
              </div>
            </div>
          </div>
          <div className="bx--col-lg-3 bx--col-md-8"></div>

        </div>
    </div>
    </div>

    {/* comment here */}
    <section className="homeAntihero">
    <div className="bx--grid">
    <div className="bx--row">
          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-5 bx--col-md-4">
              <img src={sectionImg0} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-5 bx--col-md-4">
            <div className="center">
              <h1>
                Making more fun watching the
                <br></br>
                sport of fitness.
              </h1>
              <Link className="link--primary" element={Link} to="/game">
                Start game now
              </Link>
            </div>
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
      </div>
      </section>
    <div className="bx--grid">
    {/* comment here */}
      <section className="homeSections">
      <div className="bx--row">
          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-4 bx--col-md-4">
              <h1>
                Pick 'em
              </h1>
              <p>
                Your can drag and drop the Athlete's position and make your best bet on 
                your favourite players.
              </p>
              <Link className="link--primary" element={Link} to="/game">
                Start playing 
              </Link>
          </div>
          <div className="bx--col-lg-6 bx--col-md-4">
                <img src={sectionImg1} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
      </section>

      <section className="homeSections">
      <div className="bx--row" data-type="reverse">
          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-6 bx--col-md-4">
              <img src={sectionImg2} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-4 bx--col-md-4">
              <h1>
                You changed your mind?
              </h1>
              <p>
                While the game is still active, you can always 
                re-position the Athletes and have a new opportunity 
                to improve your results.
              </p>
              <Link className="link--primary" element={Link} to="/game">
                Start playing 
              </Link>                
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
      </section>

      <section className="homeSections" data-type="no-padding-bottom">
      <div className="bx--row">
          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-4 bx--col-md-4">
              <h1>
                Wait for the final results
              </h1>
              <p>
                  Once the game is inactive, you'll be unable to modify 
                  your picks, in the meantime it's time to take a seat and 
                  cross your fingers until the finals results are ready.
              </p>
              <Link className="link--primary" element={Link} to="/game">
                Start playing
              </Link>
          </div>
          <div className="bx--col-lg-6 bx--col-md-4">
                <img src={sectionImg3} alt='website logo' className="sectionImg" />
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
      </section>
    </div>
    
    <section className="homeBanner">
    <div className="bx--grid">
      {/* comment here */}
      <div className="bx--row">
          <div className="bx--col-lg-1 bx--col-md-8"></div>
          <div className="bx--col-lg-10 bx--col-md-8">
              <h1>
                    It's time to position your 
                    <br></br>
                    favourite Athletes.
              </h1>
              <Link className="link--primary" element={Link} to="/game">
                Play now
              </Link>
          </div>
          <div className="bx--col-lg-1 bx--col-md-8"></div>
      </div>
    </div>
    </section>
    <Footer />
    </>
  );
};
export default Home;
