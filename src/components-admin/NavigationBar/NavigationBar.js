import React from 'react';
import './NavigationBar.scss';
import Logo from './logo.svg';
import ls from 'local-storage'
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Link } from 'react-router-dom';

class NavigationBarAdmin extends React.Component {
constructor(props) {
        super(props);
        const path = window.location.href.split('/').pop();
        this.state = {currentPath: path};
      }

      displayName(name){
        var result = name.split(' ');
        if(result.length > 1){
            return `${result[0]} ${result[1]}`;
        }
        
        return result[0];
      }

      displayAvatarName(name){
        var result = name.split(' ');
        if(result.length > 1){
            return `${result[0][0]}${result[1][0]}`;
        }
        
        return result[0][0];
      }

    render() {
        let session = ls.get('session');
        let name = ls.get('name');

        if(this.state.currentPath !== 'login' && !session){
            window.location.href = '/admin/login';
            return 
        }

        const renderAuthButton = ()=>{
                return (
                    <>
                    
                    <div className="sessionControl">
                    <Link element={Link} to="/admin" data-type="user">
                    {(name)
                     ?  <strong className="username--nowrap">{this.displayName(name)}</strong> 
                     : ''
                    }
                    </Link>

                    <div className="overflowMenu">
                    <OverflowMenu flipped> 
                        <OverflowMenuItem 
                            itemText="Settings" 
                            primaryFocus
                            onClick={ 
                                function profile () {
                                window.location.href='/admin/settings';
                            }}
                        />
                        <OverflowMenuItem
                            itemText="Sign out"
                            direction='top'
                            onClick={ 
                                    function logout () {
                                    window.location.href='/admin/logout';
                                }}
                            />
                    </OverflowMenu>
                    </div>
                    {/* User account */}
                    <div className="account-dropdown">
                        <div className="avatar">
                            <span>
                            {(name)
                             ? this.displayAvatarName(name)
                             : ''
                            }
                            </span>
                        </div>
                    </div>
                    </div>
                    </>
                )
            }

        return (
            <>
            {(session) ?
                <div className="navBar">
                    <Link element={Link} to="/admin" data-type="logo">
                        <img 
                            src={Logo} 
                            alt="Admin Pick The Fittest"  
                            data-type="logo" 
                        />
                    </Link>
                    <ul>
                        <Link element={Link} to="/admin/games">
                            <li>Games</li>
                        </Link>
                        <Link element={Link} to="/admin/athletes">
                            <li>Athletes</li>
                        </Link>
                        <Link element={Link} to="/admin/members">
                            <li>Members</li>
                        </Link>
                        <Link element={Link} to="/admin/members">
                            <li>Groups</li>
                        </Link>
                        <Link element={Link} to="/admin/leaderboard-list">
                            <li>Leaderboard</li>
                        </Link>
                    </ul>
                    {renderAuthButton()}
                </div>
            :
                <div>
                    <Link element={Link} to="/admin">
                        <img src={Logo} alt='website logo' className="logo" />
                    </Link>
                </div>
            }
            </>
        )
    }
}
export default NavigationBarAdmin;
