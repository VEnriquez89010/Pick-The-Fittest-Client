import React from "react";
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';

const Breadcrumbs = ({ breadcrumbs }) => (
  <React.Fragment>
    {breadcrumbs.map(({ breadcrumb }) => breadcrumb)}
  </React.Fragment>
)

export default withBreadcrumbs()(Breadcrumbs);