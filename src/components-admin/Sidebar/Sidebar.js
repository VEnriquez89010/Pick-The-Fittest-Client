import React from "react";
import './Sidebar.scss?v=2.0.0';
import Logo from './logo.svg';
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import ls from 'local-storage'


class SidebarAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
      session: ls.get('session')
    };
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  onSetSidebarOpen(open) {
    document.body.style.overflow =  (open) ? "hidden" : "";
    this.setState({ sidebarOpen: open });
  }
  

  render() {
    const renderAuthButton = ()=>{
      if(this.state.session !== null){
        return <Link element={Link} to="/admin/logout">Log out</Link> 
      }
    }

    if(this.state.session){
      return (
        <>
        <div className="menuMobile">
          <div className="burguerMenu" 
               onClick={() => this.onSetSidebarOpen(true)}>
          </div>
        </div>
        <Sidebar
          sidebar={
              <div className="SidebarContainer">
              <ul className="Sidebar">
                  <li><Link element={Link} to="/admin/games">Games</Link></li>
                  <li><Link element={Link} to="/admin/athletes">Athletes</Link></li>
                  <li><Link element={Link} to="/admin/members">Members</Link></li>
                  <li><Link element={Link} to="/admin/members">Groups</Link></li>
                  <li><Link element={Link} to="/admin/leaderboard-list">Leaderboard</Link></li>
                  <li><Link element={Link} to="/admin/signup">Add account</Link></li>
                  <li><Link element={Link} to="/admin/settings">Settings</Link></li>
                  <li>
                    {renderAuthButton()}
                  </li>
              </ul>

              </div>
          }
          open={this.state.sidebarOpen}
          onSetOpen={this.onSetSidebarOpen}
          shadow={false}
          styles={{ 
              root: { },
              sidebar: { 
                  position: "absolute",
                  background: "rgba(25,25,25, 0.70)", 
                  backdropFilter: "saturate(180%) blur(20px)",
                  WebkitBackdropFilter: "saturate(180%) blur(20px)",
                  width: "260px",
                  zIndex: 99999999,
                  transition: "transform .2s ease-out",
                  WebkitTransition: "-webkit-transform .2s ease-out"
              },
              content: { overflowY: 'auto' },
              overlay: {
                  backgroundColor: "rgba(25,25,25, 0.50)",
                  overflow: "hidden",
                  zIndex: 999,
                  transition: "opacity .1s ease-out, visibility .1s ease-out",
              }
          }}
          >
        </Sidebar>
        </>
      );
    }else{
      return null;
    }
  }
}

export default SidebarAdmin;
