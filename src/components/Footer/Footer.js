import React from 'react';
import './Footer.scss?v=2.0.0';
import Logo from './logo.svg';
import footerIcon from './icon-globe.svg';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Soccer32 } from '@carbon/icons-react';
import ls from 'local-storage'

const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/login">Sign in</Link> 
  } else{
    return <Link element={Link} to="/logout">Sign out</Link> 
  }
}

const footerAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Soccer32}>Sign up now</Button></Link>
  } else{
    return <Link element={Link} to="/profile"><Button renderIcon={Soccer32}>Start playing</Button></Link>
  }
}


const Footer = () => {
    return (

      <>
      <footer className="footer">
      <div className="bx--grid">
          <div className="bx--row ">
              <div className="bx--col-lg-1 bx--col-md-8"></div>
              <div className="bx--col-lg-4 bx--col-md-4">
                <div className="footerContact">
                  <img src={Logo} alt='website logo' className="footerLogo" />
                  <h3>
                      The place where you can predict the top finishers for different fitness competitions.
                  </h3>
                  <br></br>
                  {footerAuthButton()}
                </div>
              </div>


              <div className="bx--col-lg-3 bx--col-md-4">
                  <ul className="footerMenu">
                    
                     {/* <li><Link element={Link} to="/results">Results</Link></li> */}
                    <li><Link element={Link} to="/leaderboard">Leaderboard</Link></li>
                     {/* <li><Link element={Link} to="/groups">Groups</Link></li> */}
                    <li><Link element={Link} to="/how-to-play">How to play</Link></li>
                    <li><Link element={Link} to="/rules">Rules</Link></li>
                    <li>{renderAuthButton()}</li>
                  </ul>
              </div>
            
              <div className="bx--col-lg-3 bx--col-md-4">
                <div className="footer--block--3">
                  <h1>Pick the Fittest</h1>
                  <p>
                      Making more fun watching the sport of fitness.
                  </p>
                  <ul className="socialNetworks">
                    <li>
                      <a href="https://www.instagram.com">
                        <img src={IconInstagram} alt='' />  
                      </a>
                    </li>
                    <li>
                      <a href="https://facebook.com">
                        <img src={IconFacebook} alt='' />  
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="bx--col-lg-1 bx--col-md-8"></div>
            </div>

            <div className="bx--row">
              <div className="footerBottom">
                <div className="bx--col-lg-1 bx--col-md-8"></div>
                <div className="bx--col-lg-5 bx--col-md-8">
                    <div className="footerLegal">
                    <img src={footerIcon} alt="SSL" className="footerIcon" />
                    <p>{new Date().getFullYear()} Pick the Fittest <span>|</span> CrossFit® is a registered 
                        <br></br>
                        trademark of CrossFit, Inc.</p>
                    </div>
                </div>
                <div className="bx--col-lg-5 bx--col-md-8">
                    <div className="footerLegalLinks">
                    <Link element={Link} to="/terms-and-conditions">
                      Terms and conditions
                    </Link>
                    </div>
                </div>
                <div className="bx--col-lg-1 bx--col-md-8"></div>
              </div>
            </div>

      </div>
      </footer>
      </>
    );
  };
  export default Footer;