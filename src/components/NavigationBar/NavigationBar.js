import React from 'react';
import './NavigationBar.scss';
import Logo from './logo.svg';
import ls from 'local-storage'
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Link } from 'react-router-dom';

class NavigationBar extends React.Component {
    displayName(name){
        var result = name.split(' ');
        if(result.length > 1){
            return `${result[0]} ${result[1]}`;
        }
        
        return result[0];
      }

      displayAvatarName(name){
        var result = name.split(' ');
        if(result.length > 1){
            return `${result[0][0]}${result[1][0]}`;
        }
        
        return result[0][0];
      }
    render() {
        const renderAuthButton = ()=>{
            let session = ls.get('session');
            let name = ls.get('name');
            if(session == null){
                return <Link element={Link} to="/login" className="logIn" >Sign in</Link> 
            } else {
                return (
                    <>
                    <div className="sessionControl">
                    <Link element={Link} to="/profile" data-type="user">
                    {(name)
                     ?  <strong className="username--nowrap">{this.displayName(name)}</strong> 
                     : ''
                    }
                    </Link>
                    <div className="overflowMenu">
                    <OverflowMenu flipped> 
                        <OverflowMenuItem 
                        itemText="My game" 
                        primaryFocus
                        onClick={ function profile () {
                            window.location.href='/profile';
                        }}
                        />
                        <OverflowMenuItem 
                            itemText="Settings" 
                            primaryFocus
                            onClick={ 
                                function profile () {
                                window.location.href='/settings';
                            }}
                        />
                        <OverflowMenuItem
                            itemText="Sign out"
                            direction='bottom'
                            onClick={ function logout () {
                                window.location.href='/logout';
                            }}
                        />
                    </OverflowMenu>
                    </div>
                    <div className="account-dropdown">
                        <div className="avatar">
                            <span>
                                {(name)
                                ? this.displayAvatarName(name)
                                : ''
                                }
                            </span>
                        </div>
                        </div>
                    </div>
                    </>
                )
            }
        }

        return (
            <>
            <div className="navBar">
                <Link element={Link} to="/" data-type="logo">
                    <img 
                        src={Logo} 
                        alt="Pick The Fittest"  
                        data-type="logo" 
                    />
                </Link>
                <ul>
                    {/* 
                    <Link element={Link} to="/results">
                        <li>Results</li>
                    </Link>
                    */}
                    <Link element={Link} to="/leaderboard">
                        <li>Leaderboard</li>
                    </Link>
                    {/* 
                    <Link element={Link} to="/">
                        <li>Groups</li>
                    </Link>
                    */}
                    <Link element={Link} to="/how-to-play">
                        <li>How to play</li>
                    </Link>
                    <Link element={Link} to="/rules">
                        <li>Rules</li>
                    </Link>
                </ul>
                {renderAuthButton()}
            </div>
            </>
        )
    }
}
export default NavigationBar;
