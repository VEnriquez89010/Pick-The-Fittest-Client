import React from "react";
import './Sidebar.scss';
import Logo from './logo.png';
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import ls from 'local-storage'



class SidebarMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
    };
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  onSetSidebarOpen(open) {
    document.body.style.overflow =  (open) ? "hidden" : "";
    this.setState({ sidebarOpen: open });
  }
  

  render() {
    const renderAuthButton = ()=>{
      let session = ls.get('session');
      if(session == null){
        return <Link element={Link} to="/login">Sign in</Link> 
      } else{
        return <Link element={Link} to="/logout">Sign out</Link> 
      }
    }

    return (
      <>
        <div className="menuMobile">
          <div className="burguerMenu" 
               onClick={() => this.onSetSidebarOpen(true)}>
          </div>
        </div>
      <Sidebar
        sidebar={
            <div className="SidebarContainer">
            <ul className="Sidebar">
                <li><Link element={Link} to="/results">Results</Link></li>
                <li><Link element={Link} to="/leaderboard">Leaderboard</Link></li>
                <li><Link element={Link} to="/how-to-play">How to play</Link></li>
                <li><Link element={Link} to="/rules">Rules</Link></li>
                <li><Link element={Link} to="/settings">Settings</Link></li>
                <li>
                  {renderAuthButton()}
                </li>
            </ul>

            </div>
        }
        open={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
        shadow={false}
        styles={{ 
            root: { },
            sidebar: { 
                position: "absolute",
                background: "rgba(25,25,25, 0.70)", 
                backdropFilter: "saturate(180%) blur(20px)",
                WebkitBackdropFilter: "saturate(180%) blur(20px)",
                width: "260px",
                zIndex: 99999,
                transition: "transform .2s ease-out",
                WebkitTransition: "-webkit-transform .2s ease-out"
            },
            content: { overflowY: 'auto' },
            overlay: {
                backgroundColor: "rgba(25,25,25, 0.50)",
                overflow: "hidden",
                zIndex: 999,
                transition: "opacity .1s ease-out, visibility .1s ease-out",
            }
        }}
        >
        <div className="burguerMenu" 
             onClick={() => this.onSetSidebarOpen(true)} 
        >
        </div>
      </Sidebar>
      </>
    );
  }
}

export default SidebarMenu;
