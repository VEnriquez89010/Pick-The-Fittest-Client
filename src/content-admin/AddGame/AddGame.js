import React from 'react';
import './AddGame.scss';
import ls from 'local-storage'
import { 
  Breadcrumb, 
  BreadcrumbItem, 
  Toggle,
  Button , TextInput , DatePicker, DatePickerInput} from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';
import arrayMove from 'array-move';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Name',
  placeholder: 'Game name'
};

const LocationInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Location',
  placeholder: 'Location'
};


const athleteType = [
  {
    Id : 0,
    Name: 'Men'
  },
  {
    Id : 1,
    Name: 'Women'
  },
  {
    Id : 2,
    Name: 'Teams'
  }
]

class AddGame extends React.Component {
   
  constructor(props) {
    super(props);

    let t = props.location.pathname.split('/');
    var id = '';
    console.log(t[3])
    if(t[3]){
      id = t.pop();
    }

    let session = ls.get('token');
   
    if (!session) {
      window.location.href='/login';
      return;
    }

    this.state = {
      name: '',
      items: [],
      tabSelected: '0',
      finalItems: [],
      location: '',
      gameId: id, 
      startDate : '',
      endDate : '',
      isActive: true,
      status: '',
      buttonDisabled: false
    };

    this.save = this.save.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
  }

  componentDidMount() {
    this.getInfo();
  }
  

  onSortEnd = ({oldIndex, newIndex}) => {
    var backupList =this.state.finalItems;
    var tabSelected = this.state.tabSelected;
  
    
    var tempList = this.state.finalItems[tabSelected];
    backupList[tabSelected] = arrayMove(tempList, oldIndex, newIndex);
    
    this.setState({finalItems: backupList, items: backupList[tabSelected] })
  };

  async getInfo(){
    var men, women, teams = '';

  var games = (this.state.gameId)
  ? await axios.get(`${appSettings.SERVER_URL}/games/id/${this.state.gameId}`, getToken())
  : await axios.get(`${appSettings.SERVER_URL}/admin/athletes/all`, getToken());

  if(this.state.gameId){
    if(games.data){
      men = games.data.Game[0];
      women = games.data.Game[1];
      teams = games.data.Game[2];
      this.setState({name: games.data.Name, location: games.data.Location, startDate: games.data.StartDate, endDate: games.data.EndDate, isActive: games.data.IsActive, status: games.data.Status });
    }
  }else{
      var athletes = await axios.get(`${appSettings.SERVER_URL}/admin/athletes/all`, getToken());
      men = athletes.data.filter(x => x.AthleteType === athleteType[0].Name);
      women = athletes.data.filter(x => x.AthleteType === athleteType[1].Name);
      teams = athletes.data.filter(x => x.AthleteType === athleteType[2].Name);
      this.setState({isActive: true, status: 'ACTIVE'});
  }

    this.setState({ items: men, finalItems: [men , women, teams] });
  } 

  onDateChange(dates){
    this.setState({startDate: dates[0], endDate:dates[1]});
  };

  changeTab(id){
    var defaulAthleteTypes =  this.state.finalItems[id] 
    this.setState({tabSelected: id, items: defaulAthleteTypes});
  }

  async save(){
    this.setState({isActive: false, buttonDisabled: true})
    var data = { Name: this.state.name, StartDate: this.state.startDate, EndDate: this.state.endDate, Location: this.state.location, Game: this.state.finalItems, Status: this.state.status };

    var result = (this.state.gameId)
    ? await axios.post(`${appSettings.SERVER_URL}/games/edit/${this.state.gameId}`, data, getToken())
    : await axios.post(`${appSettings.SERVER_URL}/games/add`, data, getToken());
 
    if(result.data.created){
      toast("Saved");
      setTimeout( () => { window.location.href = '/admin/games' }, 4000);
      return;
    }

    if(result.data.msg && result.data.msg === 'No Atlhetes'){
      toast("No atlhetes found");
      setTimeout( () => { window.location.href = '/admin/add-athlete' }, 4000);
      return;
    }
    
    toast("Not saved");
    this.setState({buttonDisabled: false})
    setTimeout( () => { window.location.href = '/admin/games' }, 4000);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  render() {
    return (
              <>
              <div className="dashBoard">
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-sm-4 bx--col-md-2"></div>
                  <div className="bx--col-sm-4 bx--col-md-4">
                      <div>
                          <h1>{this.state.name}</h1>
                      </div>
                  </div>
                  <div className="bx--col-sm-4 bx--col-md-2"></div>
                  </div>
                  
                  <div className="bx--row">
                  <div className="bx--col-sm-4 bx--col-md-2"></div>
                  <div className="bx--col-sm-4 bx--col-md-4">
                    <div className="trainerRow" data-type="border-top">
                      <div>
                        <Toggle
                          labelText="Game status"
                          labelA= "Inactive"
                          labelB= "Active"
                          aria-label="toggle button"
                          toggled={this.state.status === 'ACTIVE'}
                          onChange={ () => this.setState({ status: (this.state.status === 'ACTIVE') ? 'PAUSE': 'ACTIVE'})}
                          id="toggle-1"
                        />
                      </div>
                      </div>
                  </div>
                  <div className="bx--col-sm-4 bx--col-md-2"></div>
                  </div>

                  <div className="bx--row">
                    <div className="bx--col-sm-4 bx--col-md-2"></div>
                    <div className="bx--col-sm-4 bx--col-md-4">
                      <TextInput
                          name='name'
                          type="text" 
                          value={this.state.name}
                          onChange={this.handleChange} 

                          {...NameInputProps}  
                        />
                        <TextInput
                          name='location'
                          type="text"
                          labelText="Location"  
                          value={this.state.location} 
                          onChange={this.handleChange} 

                          {...LocationInputProps}  
                        />
                        <DatePicker dateFormat="m/d/Y" datePickerType="range" value={[this.state.startDate, this.state.endDate]} onChange={this.onDateChange}>
                          <DatePickerInput
                            id="date-picker-range-start"
                            placeholder="mm/dd/yyyy"
                            labelText="Start"
                            type="text"
                            data-type="no--margin" 
                            onClick={() => this.setState({startDate: ''})}
                          />
                          <DatePickerInput
                            id="date-picker-range-end"
                            placeholder="mm/dd/yyyy"
                            labelText="End"
                            type="text"
                            data-type="no--margin" 
                            onClick={() => this.setState({endDate: ''})}
                          />
                        </DatePicker>
                          
                        <br></br>
                        <br></br>
                          <Button type="submit" value="Submit" className="buttonAccess" onClick={this.save} disabled={this.state.buttonDisabled}>
                              {(this.state.gameId) ? 'Update' : 'Save'}
                          </Button>
                    </div>
                    <div className="bx--col-sm-4 bx--col-md-2"></div>
                  </div>

                  <Breadcrumb className="breadCrumb">
                    <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
                    <BreadcrumbItem href="/admin/games">Games</BreadcrumbItem>
                    <BreadcrumbItem isCurrentPage href="/admin/game-creator">Add game</BreadcrumbItem>
                  </Breadcrumb>

                </div>
              </div>
              </>
            );
};
}

export default AddGame;
