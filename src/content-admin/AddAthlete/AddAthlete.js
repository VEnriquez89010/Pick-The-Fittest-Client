import React from 'react';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import './AddAthlete.scss';
import avatarDefault from './avatar-default.png';
import { 
  Breadcrumb, BreadcrumbItem,
  Form, 
  TextInput, 
  Button, 
  Dropdown,
  FileUploader
 } from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getToken } from '../../helpers/SessionControl';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Modal } from 'react-responsive-modal';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Name',
  placeholder: ''
};

const athleteType = [
  {
    Id : 0,
    Name: 'Men'
  },
  {
    Id : 1,
    Name: 'Women'
  },
  {
    Id : 2,
    Name: 'Teams'
  }
]

class AddAthleteForm extends React.Component {
  imageRef = null;

  constructor(props) {
    super(props);
    

    let t = props.location.pathname.split('/');
    var id = '';
    if(t[3]){
      id = t.pop();
    }

    this.state = {
      name: '', 
      description: '', 
      selectedAthleteType: '', 
      athleteId: id, 
      imageUrl: '', 
      disableButton: false, 
      selectedFile: null,
      open:false,
      src: null,
      crop: {
        unit: '%',
        width: 80,
        height: 80,
        // x: 25,
        // y: 25,
        aspect: 1 / 1
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.saveCropImage = this.saveCropImage.bind(this);
    this.onImageLoaded = this.onImageLoaded.bind(this);
  }

  componentDidMount() {
    if(this.state.athleteId){
      axios.get(`${appSettings.SERVER_URL}/admin/athletes/id/${this.state.athleteId}`)
      .then(athlete =>{
        let { Description, Name, ImagePath, AthleteType } = athlete.data;
        this.setState({ name: Name, description: Description, imageUrl: ImagePath, selectedAthleteType: AthleteType})
      }  )
      .catch(error => this.setState({ error, isLoading: false }));
    }
  }

  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
      this.setState({ open: true, selectedFile: e.target.files[0]})
    }
  };

  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    this.setState({croppedImageUrl: ''});
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        'newFile.jpeg'
      );
      this.setState({ croppedImageUrl});
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          //reject(new Error('Canvas is empty'));
          console.error('Canvas is empty');
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, 'image/jpeg');
    });
  }

   onClickHandler = async (e) => {
    e.preventDefault();

    if(!this.state.name){
      toast('Add a name');
      return;
    }

    if(!this.state.selectedAthleteType){
      toast('Select a category');
      return;
    }

    this.setState({disableButton: true});

    let values =  { Name: this.state.name, Description: this.state.description, AthleteType: this.state.selectedAthleteType } 

    var imageFile = await this.blobToFile();
     var data = new FormData()
     data.append('athlete', JSON.stringify(values));
     data.append('file', imageFile);

    
    axios.put(`${appSettings.SERVER_URL}/admin/athletes/${this.state.athleteId}`, data, getToken())
    .then(() => {
      let msg = (this.state.athleteId) ? 'Athlete modified' : 'Athlete added' ;
      toast(msg);
      setTimeout(() => {
      window.location.href = '/admin/athletes'
      }, 2000);
    }).catch();
  }

  async blobToFile(){
    var name = this.state.selectedFile ?  this.state.selectedFile.name : 'png';
    var image = this.state.croppedImageUrl ?  this.state.croppedImageUrl: avatarDefault;
    console.log(image)
    return await fetch(image).then(r => r.blob()).then(blobFile => new File([blobFile], name, { type: "image/*" }));
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  setAthleteType(type){
    let athlete =  athleteType.find(x => x.Name === type);
    this.setState({ selectedAthleteType: athlete.Name});
  }

  saveCropImage(){
    this.setState({ open: false });
    if(!this.state.croppedImageUrl){
      var crop =  { width: this.imageRef.width, height: this.imageRef.height, x: 0, y: 0 };
      this.makeClientCrop(crop);
      return;
    }
  }

  onCloseModal(){
    this.setState({ open: false, croppedImageUrl: null});
  }

  render() {
    const { crop, croppedImageUrl, src , open} = this.state;
    return (
      <>
        <Modal 
            open={open} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
        >
        <div>
          <p>
            <strong>
              Profile picture
            </strong>
          </p>
          {src && (
          <ReactCrop
            src={src}
            crop={this.state.crop}
            ruleOfThirds
            onImageLoaded={this.onImageLoaded}
            onComplete={this.onCropComplete}
            onChange={this.onCropChange}
          />
        )}
          <Button 
            type="submit" 
            value="Submit" 
            size='small'
            kind='ghost' 
            onClick={this.saveCropImage}
          >
            Confirm
          </Button>
          <Button 
            type="submit" 
            kind='ghost' 
            size='small'
            onClick={this.onCloseModal} 
          >
            Cancel
          </Button>
        </div>
        </Modal>

      <div className="dashBoard">
      <div>
   
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form>

              <h1 className="fileUploaderLabel">
                {(!this.state.athleteId) 
                ? 'Add'
                : 'Edit'} athlete
              </h1>

              {(croppedImageUrl) 
              ?
                <img alt="Crop" className="instructorAvatar"  style={{ maxWidth: '100%' }} src={croppedImageUrl} />
              :
              (this.state.imageUrl) ?
                <img className="instructorAvatar" src={this.state.imageUrl} alt='Pick The Fittest'></img>
              : 
                <img className="instructorAvatar" src={avatarDefault} alt='Pick The Fittest'></img>
              }


              <div className="bx--file__container">
                <FileUploader
                  accept={[
                    '.jpg',
                    '.png'
                  ]}
                  buttonKind="secondary"
                  buttonLabel="Upload a photo"
                  filenameStatus="edit"
                  iconDescription="Clear file"
                  labelDescription="only .jpg & .png files at 50mb or less"
                  // labelTitle="Profile pic"
                  onChange={this.onSelectFile} 
                  onClick={(event)=> {  event.target.value = null }}
                  className="upload--file"
                />
              </div>
              
              
              <div className="bx--file__container">
                {/* <FileUploader
                  accept={[
                    '.jpg',
                    '.png'
                  ]}
                  buttonKind="secondary"
                  buttonLabel="Upload pic"
                  filenameStatus="edit"
                  iconDescription="Clear file"
                  labelDescription="Only .jpg .png 50mb max."
                  labelTitle="Profile pic"
                  onChange={this.onChangeHandler}
                /> */}
              </div>
              <TextInput
              name='name'
              type="text" 
              required
              value={this.state.name} 
              onChange={this.handleChange} 
              {...NameInputProps}  
              />

              <Dropdown 
                items={athleteType.map(x => x.Name)} 
                value={this.state.selectedAthleteType} 
                label="Select Athlete Type"  
                name="athleteType"  
                titleText="Category"
                required
                onChange={({ selectedItem }) => this.setAthleteType(selectedItem)}
                selectedItem={this.state.selectedAthleteType} 
              />
              <br></br>
              <Button 
                  type="submit" 
                  value="Submit" 
                  className="buttonAccess"
                  size="small"
                  kind="secondary"
                  disabled={this.state.disableButton}
                  onClick={this.onClickHandler}
              >
                  Save
              </Button>

          </Form>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>

      <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem href="/admin/athletes">Athletes</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/add-athlete">Add athlete</BreadcrumbItem>
      </Breadcrumb>

      </div>
      </div>
      </div>
      </>
    );
  }
}

export default AddAthleteForm;