import React from 'react';
import './Games.scss';
import { Breadcrumb, BreadcrumbItem, Button } from 'carbon-components-react';
import { 
  GameConsole32, 
  ClassifierLanguage32
 } from '@carbon/icons-react';
import { MisuseOutline32  , TrashCan32} from '@carbon/icons-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import appSettings from '../../helpers/AppSettings';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getToken } from '../../helpers/SessionControl';

class Games extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      open: false,
      selected: '',
      currentGame: {},
      history: [],
      GameStatus: ''
    };

    this.changeGameStatus = this.changeGameStatus.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
  }


  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  async componentDidMount() {
    var history = await axios.get(`${appSettings.SERVER_URL}/games/history`, getToken());
    var currentActive = await axios.get(`${appSettings.SERVER_URL}/games/current-active`, getToken());

    if(currentActive.data){
      this.setState({ currentGame: currentActive.data});
    }

    this.setState({history: history?.data ?? {}});
  }

  edit(id){
    window.location.href=`/admin/game-creator/${id}`;
  }

  setInstructor(id){
    this.onOpenModal();
    this.setState({selected: id});
  }

  async changeGameStatus(){
    var result =  (this.state.currentGame.Status === 'ACTIVE') 
    ? await axios.get(`${appSettings.SERVER_URL}/games/change-status/${this.state.currentGame.Id}/PAUSE`, getToken())
    : await axios.get(`${appSettings.SERVER_URL}/games/change-status/${this.state.currentGame.Id}/CLOSE`, getToken())

    if(result.data){
      toast("Game Status Updated");
      this.setState({ IsGameActive: true});
      return;
    }

    toast("Not Updated");
  }

  results(id){
    window.location.href=`/admin/leaderboard/${id}`;
  }



  render() {
    const { open } = this.state;
    return (
    <>
     <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              >
              <p className="modal--text">
               <strong>
                  Are you sure?
               </strong>
              </p>
             <Button 
                renderIcon={MisuseOutline32 }  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.onCloseModal}
              >
                Cancel
              </Button>
              <Button 
                renderIcon={TrashCan32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.delete(this.state.selected)}
              >
                Delete
              </Button>

            </Modal>
    <div className="dashBoard">
    <div>
            <ToastContainer 
            position="top-right"
            />
            
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
            <h1>Games</h1>
            <br></br>
            <br></br>
          </div>
          <div className="bx--col-md-2"></div>
        </div>
            
        <div className="bx--row">
            <div className="bx--col-md-2"></div>
            <div className="bx--col-md-4">
              { (this.state.currentGame.Id) ?
                <div 
                className="packBox"
                onClick={() => this.edit(this.state.currentGame.Id)}
                >

                <div className="packContent">
                  <h4>Edit Game</h4>
                  <div className="packContentBottom">
                    <GameConsole32 className="BoxIcon" />
                  </div>
                  <div className="status--dot">
                    {this.state.currentGame.Status  === 'PAUSE'
                            ? <p>
                                <span data-type="inactive"></span> 
                                Inactive 
                              </p>
                            : <p>
                                <span data-type="active"></span> 
                                Active 
                              </p>
                            }
                  </div>
                </div>
  
              </div> 
            : 
              <div  className="packBox"
                    onClick={ () =>  window.location.href=`/admin/game-creator` }
                >

                <div className="packContent">
                  <h4>Add Game</h4>
                  <div className="packContentBottom">
                    <GameConsole32 className="BoxIcon" />
                  </div>
                  <div className="status--dot"> <p>
                      <span data-type="inactive"></span> 
                      Inactive 
                    </p>
                           
                            
                  </div>
                </div>

                </div> 
            }

           </div>

           <div className="bx--col-md-2"></div>
           <div className="bx--col-md-2"></div>
           {(this.state.currentGame.Status === 'PAUSE') ?
            <div className="bx--col-md-4">
            <div 
                className="packBox"
                onClick={() =>  window.location.href=`/admin/add-results/${this.state.currentGame.Id}`}
            >

            <div className="packContent">
              <h4>Add Results</h4>
              <div className="packContentBottom">
                            <ClassifierLanguage32 className="BoxIcon" />
              </div>
            </div>

            </div> 
            </div>
           :
            <div className="bx--col-md-4">
            <div 
                className="packBox disable"
            >
      
            <div className="packContent">
              <h4>Add Results</h4>
              <div className="packContentBottom">
                            <ClassifierLanguage32 className="BoxIcon" />
              </div>
            </div>
     
            </div> 
            </div>
          }
         
          <div className="bx--col-md-2"></div>

         
          </div>


        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
            <h4>History</h4>
            <h6>Previous games results</h6>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4">
                      <div className="trainerRow">
                      <p data-type='title'>
                          Name
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Location
                      </p>
                      </div>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.history.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4 ">
                  <div className="trainerRow">
                  <p
                    className="instructorsLink"
                    onClick={() => this.results(element.GameId)}
                  >
                      {element.Name}
                  </p>
                  <p className="city" >
                  {element.Location}
                  </p>
                  </div>
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }
          <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/Games">Games</BreadcrumbItem>
          </Breadcrumb>
      </div>
    </div>
    </div>

    </>
    );
  }
};
export default Games;
