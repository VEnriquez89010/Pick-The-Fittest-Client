import React from 'react';
import './Athletes.scss?v=2.0.0';
import { Link } from 'react-router-dom';
import { 
  Breadcrumb, 
  BreadcrumbItem, 
  OverflowMenu, 
  OverflowMenuItem 
} from 'carbon-components-react';
import { Button } from 'carbon-components-react';
import { UserFollow32 } from '@carbon/icons-react';
import { MisuseOutline32  , TrashCan32} from '@carbon/icons-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Athletes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      open: false,
      selected: ''
    };

    this.delete = this.delete.bind(this);
  }


  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/admin/athletes/all`)
      .then(response => this.setState({ data: response.data }))
      .catch();
  }


  async delete(){
    this.setState({open: false});

    var latestGame = await axios.get(`${appSettings.SERVER_URL}/game-results/latest`);
    
    if(latestGame.data.IsActive){
      toast("You are not able to delete a Athlete until game is completed");
      return;
    }
    
    var result = await axios.delete(`${appSettings.SERVER_URL}/admin/athletes/${this.state.selected}`);
  
    if(result.data){
      toast("Athlete deleted.");
      this.componentDidMount();
      return;
    }else{
      //---- Error
      toast("Athlete was not deleted.");
    }
  }

  edit(id){
    window.location.href=`/admin/add-athlete/${id}`;
  }

  setAthlete(id){
    this.onOpenModal();
    this.setState({selected: id});
  }


  render() {
    const { open } = this.state;
    return (
    <>
     <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              modalId={3} 
              >
             <p className="modal--text">
               <strong>
                  Are you sure you want to delete this Athlete?
               </strong>
              </p>
             <br></br>
             <Button 
                renderIcon={MisuseOutline32 }  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.onCloseModal}
              >
                Cancel
              </Button>
              <Button 
                renderIcon={TrashCan32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.delete()}
              >
                Delete
              </Button>

            </Modal>
    <div className="dashBoard">
    <div>
           
            
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
            <div className="flex">
                  <h1>Athletes</h1>
                  <Link element={Link} to="/admin/add-athlete">
                      <Button 
                        size="small"
                        kind="secondary"
                        renderIcon={UserFollow32}
                      >
                        Add
                      </Button>
                  </Link>
            </div>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4">
                    <div className="trainerRow">
                      <p data-type='title'>
                          Name
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Modify
                      </p>
                      </div>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.data.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4">
                  <div className="trainerRow">
                  <p 
                    className="instructorsLink"
                    onClick={() => this.edit(element.Id)}>
                      {element.Name}
                  </p>
                  <OverflowMenu className="overflowMenu" flipped>
                  <OverflowMenuItem 
                      itemText="Edit" 
                      onClick={() => this.edit(element.Id)}
                  />
                  <OverflowMenuItem 
                      itemText="Delete" 
                      hasDivider 
                      isDelete 
                      onClick={() => this.setAthlete(element.Id)}
                  />
                  </OverflowMenu>
                  </div>
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }
          <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/athletes">Athletes</BreadcrumbItem>
          </Breadcrumb>
      </div>
    </div>
    </div>
    </>
    );
  }
};
export default Athletes;
