import React from 'react';
import './LeaderboardList.scss';
import { Breadcrumb, BreadcrumbItem, Button } from 'carbon-components-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getToken } from '../../helpers/SessionControl';

class Games extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      open: false,
      selected: '',
      currentGame: {},
      history: [],
      GameStatus: ''
    };

    this.changeGameStatus = this.changeGameStatus.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
  }


  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  async componentDidMount() {
    var history = await axios.get(`${appSettings.SERVER_URL}/games/history`, getToken());
    var currentActive = await axios.get(`${appSettings.SERVER_URL}/games/current-active`, getToken());

    if(currentActive.data){
      this.setState({ currentGame: currentActive.data});
    }

    this.setState({history: history?.data ?? {}});
  }

  edit(id){
    window.location.href=`/admin/game-creator/${id}`;
  }

  setInstructor(id){
    this.onOpenModal();
    this.setState({selected: id});
  }

  async changeGameStatus(){
    var result =  (this.state.currentGame.Status === 'ACTIVE') 
    ? await axios.get(`${appSettings.SERVER_URL}/games/change-status/${this.state.currentGame.Id}/PAUSE`, getToken())
    : await axios.get(`${appSettings.SERVER_URL}/games/change-status/${this.state.currentGame.Id}/CLOSE`, getToken())

    if(result.data){
      toast("Game Status Updated");
      this.setState({ IsGameActive: true});
      return;
    }

    toast("Not Updated");
  }

  results(id){
    window.location.href=`/admin/leaderboard/${id}`;
  }



  render() {
    const { open } = this.state;
    return (
    <>

    <div className="dashBoard">
      <div className="bx--grid">


         
          


        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
            <h1>Leaderboard</h1>
            <h6>Previous games results</h6>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4">
                      <div className="trainerRow">
                      <p data-type='title'>
                          Name
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Location
                      </p>
                      </div>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.history.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4 ">
                  <div className="trainerRow">
                  <p
                    className="instructorsLink"
                    onClick={() => this.results(element.GameId)}
                  >
                      {element.Name}
                  </p>
                  <p className="city" >
                  {element.Location}
                  </p>
                  </div>
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }
          <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/leaderboard-list">Leaderboard</BreadcrumbItem>
          </Breadcrumb>

      </div>
    </div>
    </>
    );
  }
};
export default Games;
