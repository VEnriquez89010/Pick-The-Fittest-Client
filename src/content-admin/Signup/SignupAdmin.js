import React from 'react';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import { 
  Breadcrumb, 
  BreadcrumbItem,
  Form, 
  TextInput, 
  Button, 
  Toggle, 
  Checkbox } from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getToken } from '../../helpers/SessionControl';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Name',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Email',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Password',
  placeholder: '6 characters + 1 digit',
};


class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: '', email: '', password: '', isAdmin: true, showPass: false, buttonDisabled: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    let currentState = this;
    currentState.setState({buttonDisabled: true });
    let values =  { Name: this.state.name, Email: this.state.email, Password: this.state.password, IsAdmin: this.state.isAdmin , AdminRole: true};

    axios.post(`${appSettings.SERVER_URL}/admin/admins/add`, values, getToken())
    .then(function (response) {
      
      if(response.data.error){
        currentState.setState({buttonDisabled: false });
        toast(response.data.message);
        return;
      }

        //--- Toast succes. Usuario Añadido correctamente
        currentState.setState({name: '', email: '', password: '', isAdmin: true, });
        toast("Account created successfully");
        setTimeout(() =>  window.location.href = '/admin', 4000);
    }).catch(error => {
      //--- Error.
      toast("An unexpected error has occurred.");
    });
  }

  render() {
    return (
      <div className="dashBoard">
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Create Account
              </h3>

              <TextInput
                name='name'
                required
                type="text" 
                value={this.state.name} 
                onChange={this.handleChange} 
                {...NameInputProps}  
              />


              <TextInput
                name='email'
                type="email" 
                required
                value={this.state.email} 
                onChange={this.handleChange} 
                pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
                
                {...EmailInputProps}  
              />

              <TextInput
                name='password'
                type={ !this.state.showPass ? 'password' : 'text'}
                required
                value={this.state.password} 
                onChange={this.handleChange} 
                pattern="(?=.*\d)(?=.*[a-z]).{7,}"

                {...PasswordInputProps}  
              />
              <Checkbox 
                labelText="Show password" 
                id="checked" 
                onChange={() => this.setState({showPass: !this.state.showPass})} />

              <Toggle
                name='isAdmin'
                aria-label="toggle button"
                defaultToggled
                id="toggle"
                labelText="Admin account"
                labelA= "Inactive"
                labelB= "Active"
                value={this.state.isAdmin} 
                onChange={() => this.setState({isAdmin: !this.state.isAdmin})} 
              />
              <br></br>
              <Button 
                type="submit" 
                value="Submit" 
                className="buttonAccess" 
                disabled={this.state.buttonDisabled}>Create</Button>
          </Form>


        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>

      <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/signup">Create account</BreadcrumbItem>
      </Breadcrumb>

      </div>
      </div>
    );
  }
}

export default SignupForm;