import React from 'react';
import { Link } from 'react-router-dom';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import { Form, TextInput, Button, Checkbox } from 'carbon-components-react';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Email',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Password',
  placeholder: ''
};


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: '', showPass: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    let values =  { Email: this.state.email, Password: this.state.password }
    axios.post(`${appSettings.SERVER_URL}/sessions/admin/login`, values)
    .then(function (response) {
        if(response.data.user){
          let userID   = response.data.user.id;
          let userName = response.data.user.Name;
          let isAdmin = response.data.user.IsAdmin;
          let adminRole = response.data.user.AdminRole;
          let token = response.data.token

          ls.set('session', userID);
          ls.set('name', userName);
          ls.set('adminRole', adminRole);
          ls.set('isAdmin', isAdmin);
          ls.set('token', token);

          window.location.href='/admin';
        } else {
          //--- User not found.
          toast("Email or password incorrect.");
        }
    })
    .catch(function (error) {
       //--- Error notification
       toast("An unexpected error has occurred.");
    });
  }

  render() {
    return (
    <>
    <div className="authentication">
    <div className="bx--grid">
    <div className="centerVertically">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Sign in
              </h3>
              <p>Use your admin Account</p>

              <TextInput
                name='email'
                type="email" 
                value={this.state.value} 
                onChange={this.handleChange} 
                
                {...EmailInputProps}  
              />

              <TextInput
                name='password'
                type={ !this.state.showPass ? 'password' : 'text'}
                value={this.state.value} 
                onChange={this.handleChange} 
                
                {...PasswordInputProps}  
              />
              <Checkbox 
                labelText="Show password" 
                id="checked" 
                onChange={() => this.setState({showPass: !this.state.showPass})} 
              />
              <br></br>

              <Button type="submit" value="Submit" className="buttonAccess">Next</Button>
              {/* 
              <fieldset className="bx--fieldset">
                <Checkbox defaultChecked labelText="Recuérdame." id="checked" />
              </fieldset> 
              */}
               <Link className="forgotPass" element={Link} to="forgot-pass">
                  Forgot password?
              </Link>
          </Form>
        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>

    </div>
    </div>
    </div>
    </>
    );
  }
}

export default LoginForm;