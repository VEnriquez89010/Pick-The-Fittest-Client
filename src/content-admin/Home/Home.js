import React from 'react';
import './Home.scss';
import { Breadcrumb, BreadcrumbItem } from 'carbon-components-react';
import { 
  GameConsole32, 
  Soccer32, 
  User32,
  Events32, 
  TypePattern32, 
  UserIdentification32,  
  Settings32
} from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';

  class HomeAdmin extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        Athletes: '',
        Users: '',
        Load : false
      };
  
      this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
      let currentState = this;
      axios.all([
        axios.get(`${appSettings.SERVER_URL}/admin/dashboard`, getToken())
      ]).then(axios.spread((dashboard) => {
        let isActiveGame = false;

        if(dashboard.data.error){
          window.location.href = '/admin/login';
          return;
        }

        if(dashboard.data.Game){
           isActiveGame = dashboard.data.Game.Status == 'ACTIVE'
        }
       
          currentState.setState({ Load: true, Athletes: dashboard.data.Athletes, Users: dashboard.data.Users, IsActiveGame: isActiveGame });
      })).catch(error => this.setState({ error, isLoading: false }));
    }

    handleSubmit(){
      window.location.href = '/admin/signup';
    }

    render(){
      let { Athletes , Users } =  this.state;
      return (
      <>
      {(this.state.Load)
  ?
  <div className="dashBoard">
                <div className="bx--grid">
                <div className="bx--row">

                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='/admin/games'}
                        >

                        <div className="packContent">
                          <h1>{}</h1>
                          <h4>Games</h4>
                          <div className="packContentBottom">
                            <GameConsole32 className="BoxIcon" />
                          </div>
                          <div className="status--dot">
                            {!this.state.IsActiveGame 
                            ? <p>
                                <span data-type="inactive"></span> 
                                Inactive 
                              </p>
                            : <p>
                                <span data-type="active"></span> 
                                Active 
                              </p>
                            }
                            
                        </div>
                        </div>

                        </div> 
                    </div>
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='/admin/athletes'}
                        >

                        <div className="packContent">
                          <h1>{Athletes}</h1>
                          <h4>Athletes</h4>
                          <div className="packContentBottom">
                            <Soccer32 className="BoxIcon" />
                          </div>
                          </div>

                        </div> 
                    </div>

                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='/admin/members'}
                        >

                        <div className="packContent">
                          <h1>{Users}</h1>
                          <h4>Members</h4>
                          <div className="packContentBottom">
                              <User32 className="BoxIcon" />
                          </div>
                        </div>

                        </div> 
                    </div>
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='/admin/members'}
                        >

                        <div className="packContent">
                          <h1>{Users}</h1>
                          <h4>Groups</h4>
                          <div className="packContentBottom">
                              <Events32 className="BoxIcon" />
                          </div>
                        </div>

                        </div> 
                    </div>

                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='admin/leaderboard-list'}
                        >

                        <div className="packContent">
                          <h1>{}</h1>
                          <h4>Leaderboard</h4>
                          <div className="packContentBottom">
                            <TypePattern32 className="BoxIcon" />
                          </div>
                        </div>

                        </div> 
                    </div>

                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox" 
                             onClick={this.handleSubmit}
                        >

                        <div className="packContent">
                                    <h4>Add Account</h4>
                                    <div className="packContentBottom">
                                        <UserIdentification32 className="BoxIcon" />
                                    </div>
                                    </div>
  
                        </div> 
                    </div>

                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="packBox"
                             onClick={() =>  window.location.href='admin/settings'}
                        >

                        <div className="packContent">
                          <h4>Settings</h4>
                          <div className="packContentBottom">
                                        <Settings32 className="BoxIcon" />
                                    </div>
                        </div>

                        </div> 
                    </div>
            </div>

            <Breadcrumb className="breadCrumb">
              <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            </Breadcrumb>
            </div>
            </div>
  : null
      }
                
      </>
      );
    };
  }

export default HomeAdmin;
