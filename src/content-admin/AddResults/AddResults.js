import React from 'react';
import './AddResults.scss';
import ls from 'local-storage'
import { 
  Breadcrumb, 
  BreadcrumbItem, 
  Button
} from 'carbon-components-react';
import { Draggable24} from '@carbon/icons-react';
import avatar from './avatar-default.png';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import { toast } from 'react-toastify';
import { Modal } from 'react-responsive-modal';
import 'react-toastify/dist/ReactToastify.css';

const SortableItem = SortableElement(({ value, i }) => 
<li className="DragDropList">
{(value.ImagePath) 
  ? <img src={value.ImagePath} class="resize" alt=""/>
  : <img src={avatar} class="resize" alt=""/>
  }
  <span>{value.Name}</span>
  <span className="athlete--position--badge">{i + 1}</span>
  <Draggable24 className="iconDraggable"/>
</li> 
);

const SortableList = SortableContainer(({items}) => {
  return (
    <ul className="DragDropContainer">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} value={value} index={index} i={index} /> 
      ))}
    </ul>
  );
});

const athleteType = [
  {
    Id : 0,
    Name: 'Men'
  },
  {
    Id : 1,
    Name: 'Women'
  },
  {
    Id : 2,
    Name: 'Teams'
  }
]

class AddResults extends React.Component {
   
  constructor(props) {
    super(props);

    let t = props.location.pathname.split('/');
    var id = '';
    console.log(t[3])
    if(t[3]){
      id = t.pop();
    }

    let session = ls.get('token');
   
    if (!session) {
      window.location.href='/login';
      return;
    }

    this.state = {
      name: '',
      items: [],
      tabSelected: '0',
      finalItems: [],
      location: '',
      gameId: id, 
      startDate : '',
      endDate : '',
      isActive: false,
      open: false
    };

    this.save = this.save.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
  }

  componentDidMount() {
    this.getInfo();
  }
  

  onSortEnd = ({oldIndex, newIndex}) => {
    var backupList =this.state.finalItems;
    var tabSelected = this.state.tabSelected;
  
    
    var tempList = this.state.finalItems[tabSelected];
    console.log(backupList)
    backupList[tabSelected] = arrayMove(tempList, oldIndex, newIndex);
    
    this.setState({finalItems: backupList, items: backupList[tabSelected] })
  };

  async getInfo(){
    var men, women, teams = '';

  var games = (this.state.gameId)
  ? await axios.get(`${appSettings.SERVER_URL}/games/id/${this.state.gameId}`, getToken())
  : await axios.get(`${appSettings.SERVER_URL}/admin/athletes/all`, getToken());

  if(this.state.gameId){
    if(games.data){
      men = games.data.Game[0];
      women = games.data.Game[1];
      teams = games.data.Game[2];
      this.setState({name: games.data.Name, location: games.data.Location, startDate: games.data.StartDate, endDate: games.data.EndDate, isActive: games.data.IsActive});
    }
  }else{
      var athletes = await axios.get(`${appSettings.SERVER_URL}/admin/athletes/all`, getToken());
      men = athletes.data.filter(x => x.AthleteType === athleteType[0].Name);
      women = athletes.data.filter(x => x.AthleteType === athleteType[1].Name);
      teams = athletes.data.filter(x => x.AthleteType === athleteType[2].Name);
      this.setState({isActive: true});
  }

    this.setState({ items: men, finalItems: [men , women, teams] });
  } 

  onDateChange(dates){
    this.setState({startDate: dates[0], endDate:dates[1]});
  };

  changeTab(id){
    var defaulAthleteTypes =  this.state.finalItems[id] 
    this.setState({tabSelected: id, items: defaulAthleteTypes});
  }

  async save(){
    this.setState({isActive: false, open: false})
    var data = { Name: this.state.name, StartDate: this.state.startDate, EndDate: this.state.endDate, Location: this.state.location, Game: this.state.finalItems };

    var result = await axios.put(`${appSettings.SERVER_URL}/games/${this.state.gameId}`, data, getToken());

    if(result.data){
      toast("Saved");
      setTimeout( () => { window.location.href = '/admin/games' }, 4000);
      return;
    }
    
    toast("Not saved");
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  onCloseModal = () => {
    this.setState({ open: false})
  };


  render() {
    return (
            <>
            <Modal 
                open={this.state.open} 
                onClose={this.onCloseModal} 
                center
                animationDuration={80}
                modalId={3} 
            >
            <div>
              <p className="modal--text">
                <strong>
                  Are you sure you want to set this final result?
                </strong>
              </p>
              <Button 
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.save()}
              >
                Confirm
              </Button>
              <Button 
                type="submit" 
                kind='ghost' 
                size='small'
                onClick={this.onCloseModal}
              >
                Cancel
              </Button>
            </div>
            </Modal>


              <div className="dashBoard">
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-sm-8 bx--col-md-2"></div>
                  <div className="bx--col-sm-8 bx--col-md-4">
                    <h1>Final results</h1>
                    <br></br>
                    <br></br>
                        <Tabs
                          className="tabsPosition"
                          defaultTab={this.state.tabSelected}
                          onChange={(tabId) => { this.changeTab(tabId)}}
                        >
                          <TabList>
                            <div  className="rwt__tab">
                            <Tab tabFor="0">Men</Tab>
                            <Tab tabFor="1">Women</Tab>
                            <Tab tabFor="2">Teams</Tab>
                            </div>
                          </TabList>
                          <TabPanel tabId={this.state.tabSelected}>
                              <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />
                          </TabPanel>
                        </Tabs>

                        <Button type="submit" value="Submit" className="buttonAccess" onClick={() => this.setState({open: true})} disabled={!this.state.isActive}>
                            {(this.state.gameId) ? 'Update' : 'Save'}
                        </Button>
                  </div>
                  <div className="bx--col-sm-8 bx--col-md-2"></div>
                  </div>

                  <Breadcrumb className="breadCrumb">
                    <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
                    <BreadcrumbItem href="/admin/games">Games</BreadcrumbItem>
                    <BreadcrumbItem isCurrentPage href="/admin/add-results">Results</BreadcrumbItem>
                  </Breadcrumb>

                </div>
              </div>
              </>
            );
};
}

export default AddResults;
