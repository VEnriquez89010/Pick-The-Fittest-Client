import React from 'react';
import './Members.scss?v=2.0.0';
import { ChevronLeft32  } from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'
import { 
  Breadcrumb, BreadcrumbItem, Button, Search } from 'carbon-components-react';
import { getToken } from '../../helpers/SessionControl';

class Members extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], auxList: [], text: '' };
  }

  componentDidMount() {
      axios.get(`${appSettings.SERVER_URL}/users/all`, getToken())
      .then(users =>  this.setState({ data: users.data, auxList: users.data }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  datediff(ex) {
    var now = new Date();
    var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var expire = new Date(ex);
    return Math.round((expire-today)/(1000*60*60*24));
  }

  filter(text){
    if(text.target.value){
      var result = this.state.data.filter(x => {
        return ((x.Name) ? x.Name.toLowerCase().includes(text.target.value.toLowerCase()) : false)
        || ((x.Email)? x.Email.toLowerCase().includes(text.target.value.toLowerCase()): false)
      });

      this.setState({auxList: result})
      return;
    }

    this.setState({ auxList: this.state.data })
  }

  render(){
    return (
      <div className="dashBoard">
      <div className="">
        <div className="bx--grid">
        <div className="bx--row">
            <div className="bx--col-lg-3"></div>
            <div className="bx--col-lg-6">
            <h1>Members</h1>
            <br></br>
            <Search
                  id="search-1"
                  name='search'
                  placeHolderText="Search"
                  data-type="no--margin"
                  light
                  onChange={text => this.filter(text)}
                />
                <br></br>
            </div>
            <div className="bx--col-lg-3"></div>
          </div>

          <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4">
                      <div className="trainerRow">
                      <p data-type='title'>
                          Name
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Email
                      </p>
                      </div>
                    </div>
                    <div className="bx--col-md-2"></div>
          </div>
          
          { this.state.auxList.map( (element, index) => {
                return(
                  <div key={index} className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4">
                    <div className="trainerRow">
                    <p 
                      className="instructorsLink"
                      onClick={ () => {
                      ls.set('memberName', element.Name );
                      window.location.href=`/reservations/${element.Id}`;
                      }}
                        >{element.Name}</p>
                  
                      <p 
                        className="city"
                        onClick={ () => {
                        ls.set('memberName', element.Name );
                        window.location.href=`/reservations/${element.Id}`;
                        }}>{element.Email}</p>
                      </div>
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                );
              })
          }

        <Breadcrumb className="breadCrumb">
            <BreadcrumbItem href="/admin">Dashboard</BreadcrumbItem>
            <BreadcrumbItem isCurrentPage href="/admin/members">Members</BreadcrumbItem>
        </Breadcrumb>
        </div>

        </div>
      </div>
    );
  };
}

export default Members;
