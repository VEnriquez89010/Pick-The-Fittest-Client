# Pick The Fittest

Client

## How to Run

>>>
1. Clone project
  * git clone https://gitlab.com/VEnriquez89010/Pick-The-Fittest-Client.git

2. Install and Run Server-Api
  * [https://gitlab.com/VEnriquez89010/Pick-The-Fittest-Server.git](https://gitlab.com/VEnriquez89010/Pick-The-Fittest-Server.git)

3. Run project
  * cd Pick-The-Fittest-Client
  * npm install
  * npm start


4. Listen on port 3000, examples
  * Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
>>>

